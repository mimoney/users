package com.mimoney.users.api.validation.helper;

public interface PasswordConfirmationHelper {

  String getPassword();

  String getConfirmPassword();

}
