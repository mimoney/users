set foreign_key_checks = 0;

truncate user;
truncate user_group;
truncate resource;
truncate users_groups;
truncate user_group_invite;

set foreign_key_checks = 1;

alter table user auto_increment = 1;
alter table user_group auto_increment = 1;

insert into user (id, user_code, user_name, first_name, last_name, email, password, creation_date, update_date, active) values
(1, '1a7152b3-6ec8-4966-8471-1766e3b996b6', 'rodrigo.bacchetti', 'Rodrigo', 'Costa Bacchetti', 'bacchetti@mimoney.com', '$2a$10$ZlyDahTHRK9JJwpwjyUQM.9jVq1vcfKu7tkO0yRWMYv1eqe.2p7hi', utc_timestamp, utc_timestamp, true),
(2, 'ed5fb833-8fd4-4339-b06c-43afa7e97484', 'lorrainy.bacchetti', 'Lorrainy', 'Rebonato Bacchetti',  'lorebonato@mimoney.com', '$2a$10$ZlyDahTHRK9JJwpwjyUQM.9jVq1vcfKu7tkO0yRWMYv1eqe.2p7hi', utc_timestamp, utc_timestamp, false),
(3, '29946286-0f78-41fa-84fa-d3278d2a97c4', 'lara.bacchetti', 'Lara', 'Rebonato Bacchetti',  'lara@mimoney.com', '$2a$10$ZlyDahTHRK9JJwpwjyUQM.9jVq1vcfKu7tkO0yRWMYv1eqe.2p7hi', utc_timestamp, utc_timestamp, false),
(4, '1166150a-feee-45d4-b006-7942eef4f8d5', 'luna.bacchetti', 'Luna', 'Rebonato Bacchetti',  'luna@mimoney.com', '$2a$10$ZlyDahTHRK9JJwpwjyUQM.9jVq1vcfKu7tkO0yRWMYv1eqe.2p7hi', utc_timestamp, utc_timestamp, false);

insert into user_group (id, name, description) values
(1, 'Home', 'Group to share home expenses'),
(2, 'Weekend Party', 'Group to share weekend party expenses'),
(3, 'Vacation trip', 'Group to share vacation trip expenses');

insert into users_groups (user_id, user_group_id, admin) values
(1, 1, true), (2, 1, false), (3, 1, false), (4, 1, false),
(1, 2, false), (2, 2, true),
(1, 3, true), (2, 3, true);

insert into user_group_invite (host_id, guest_id, user_group_id, invite_code, invitation_date) values
(1, 3, 3, 'e00672a2-77b2-484c-8a8e-78fa44d22d51', utc_timestamp),
(1, 4, 3, 'dd343b17-50b0-4671-bb89-4d6e4dbe6d17', utc_timestamp);

insert into resource (user_id, id, name, value, type) values
(1, 1, 'NuBank Account', 950.0, 'BANK_ACCOUNT'),
(1, 2, 'Emergency funds', 5000.0, 'EMERGENCY_FUND'),
(2, 1, 'PicPay Account', 210.0, 'BANK_ACCOUNT');
