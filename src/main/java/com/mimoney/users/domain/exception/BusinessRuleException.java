package com.mimoney.users.domain.exception;

import com.mimoney.users.domain.enumerated.ErrorCode;
import lombok.Getter;

@Getter
public class BusinessRuleException extends RuntimeException {

  private final ErrorCode errorCode;

  public BusinessRuleException(ErrorCode errorCode) {
    super(errorCode.getMessage());
    this.errorCode = errorCode;
  }

}
