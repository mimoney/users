package com.mimoney.users.domain.service;

import com.mimoney.users.domain.model.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface IUserService {

  List<User> getUsers();

  List<User> getUsersByCodes(Set<String> userCodes);

  User getUserByCode(String userCode);

  @Transactional
  User updateUser(User user);

}
