package com.mimoney.users.api.exception;

import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;
import com.mimoney.users.domain.enumerated.ErrorCode;
import com.mimoney.users.domain.exception.BusinessRuleException;
import com.mimoney.users.domain.exception.EntityInUseException;
import com.mimoney.users.domain.exception.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
@AllArgsConstructor
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

  private static final String FAIL_LOG_TEMPLATE = "[Failed RQ] [{}] [{}] [IP: {}]";
  private final MessageSource messageSource;
  private final HttpServletRequest request;

  @ExceptionHandler(Exception.class)
  protected ResponseEntity<Object> handleUncaught(Exception ex, WebRequest request) {
    var errorCode = ErrorCode.INTERNAL_SERVER_ERROR;
    var body = getBodyFromErrorCode(errorCode, null);
    return handleExceptionInternal(ex, body, new HttpHeaders(), errorCode.getStatus(), request);
  }

  @ExceptionHandler(BusinessRuleException.class)
  protected ResponseEntity<Object> handleBusinessRuleException(BusinessRuleException ex, WebRequest request) {
    var errorCode = ex.getErrorCode();
    var body = getBodyFromErrorCode(errorCode, ex.getMessage());
    return handleExceptionInternal(ex, body, new HttpHeaders(), errorCode.getStatus(), request);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  protected ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request) {
    var errorCode = ex.getErrorCode();
    var body = getBodyFromErrorCode(errorCode, ex.getMessage());
    return handleExceptionInternal(ex, body, new HttpHeaders(), errorCode.getStatus(), request);
  }

  @ExceptionHandler(EntityInUseException.class)
  protected ResponseEntity<Object> handleEntityInUseException(EntityInUseException ex, WebRequest request) {
    var errorCode = ex.getErrorCode();
    var body = getBodyFromErrorCode(errorCode, ex.getMessage());
    return handleExceptionInternal(ex, body, new HttpHeaders(), errorCode.getStatus(), request);
  }

  @ExceptionHandler(AccessDeniedException.class)
  protected ResponseEntity<Object> handleAccessDenied(AccessDeniedException ex, WebRequest request) {
    var errorCode = ErrorCode.ACCESS_DENIED;
    var body = getBodyFromErrorCode(errorCode, null);
    return handleExceptionInternal(ex, body, new HttpHeaders(), errorCode.getStatus(), request);
  }

  @Override
  protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    var errorCode = ErrorCode.NOT_FOUND;
    var message = String.format(errorCode.getMessage(), ex.getRequestURL());

    var body = ApiError.builder()
      .errorCode(errorCode.getCode())
      .title(errorCode.getTitle())
      .message(message)
      .build();

    return handleExceptionInternal(ex, body, headers, errorCode.getStatus(), request);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    Throwable rootCause = ExceptionUtils.getRootCause(ex);

    if (rootCause instanceof InvalidFormatException) {
      return handleInvalidFormatException((InvalidFormatException) rootCause, headers, request);
    } else if (rootCause instanceof PropertyBindingException) {
      return handlePropertyBindingException((PropertyBindingException) rootCause, headers, request);
    }

    var errorCode = ErrorCode.INVALID_BODY;
    var body = ApiError.builder()
      .errorCode(errorCode.getCode())
      .title(errorCode.getTitle())
      .message(errorCode.getMessage())
      .build();

    return handleExceptionInternal(ex, body, headers, errorCode.getStatus(), request);
  }

  @Override
  protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    if (ex instanceof MethodArgumentTypeMismatchException) {
      return handleMethodArgumentTypeMismatch((MethodArgumentTypeMismatchException) ex, headers, request);
    }
    return super.handleTypeMismatch(ex, headers, status, request);
  }

  @Override
  protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    return handleValidationInternal(ex, ex.getBindingResult(), headers, request);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    return handleValidationInternal(ex, ex.getBindingResult(), headers, request);
  }

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
    ApiError apiError;

    if (Objects.isNull(body)) {
      apiError = ApiError.builder().title(status.getReasonPhrase()).build();
    } else if (body instanceof String) {
      apiError = ApiError.builder().title((String) body).build();
    } else {
      apiError = (ApiError) body;
    }

    if (apiError.isUnexpectedError()) {
      logUnexpectedError(ex);
    } else if (apiError.isValidationError()) {
      logValidationError(apiError);
    } else {
      logError(apiError);
    }

    return super.handleExceptionInternal(ex, apiError, headers, status, request);
  }

  protected ResponseEntity<Object> handlePropertyBindingException(PropertyBindingException ex, HttpHeaders headers, WebRequest request) {
    var errorCode = ErrorCode.NONEXISTENT_PROPERTY;
    var path = getPropertyPath(ex.getPath());
    var message = String.format(errorCode.getMessage(), path);

    var body = ApiError.builder()
      .errorCode(errorCode.getCode())
      .title(errorCode.getTitle())
      .message(message)
      .build();

    return handleExceptionInternal(ex, body, headers, errorCode.getStatus(), request);
  }

  protected ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex, HttpHeaders headers, WebRequest request) {
    var errorCode = ErrorCode.INVALID_PROPERTY;
    var path = getPropertyPath(ex.getPath());
    var message = String.format(errorCode.getMessage(), path, ex.getValue(), ex.getTargetType().getSimpleName());

    var body = ApiError.builder()
      .errorCode(errorCode.getCode())
      .title(errorCode.getTitle())
      .message(message)
      .build();

    return handleExceptionInternal(ex, body, headers, errorCode.getStatus(), request);
  }

  protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, HttpHeaders headers, WebRequest request) {
    var errorCode = ErrorCode.INVALID_PARAM;
    var message = String.format(errorCode.getMessage(), ex.getName(), ex.getValue(), Objects.requireNonNull(ex.getRequiredType()).getSimpleName());

    var body = ApiError.builder()
      .errorCode(errorCode.getCode())
      .title(errorCode.getTitle())
      .message(message)
      .build();

    return handleExceptionInternal(ex, body, headers, errorCode.getStatus(), request);
  }

  private ResponseEntity<Object> handleValidationInternal(Exception ex, BindingResult bindingResult, HttpHeaders headers, WebRequest request) {
    var errorCode = ErrorCode.INVALID_DATA;
    var errorObjects = bindingResult.getAllErrors().stream().map(this::getApiErrorObject).collect(Collectors.toList());

    var body = ApiError.builder()
      .errorCode(errorCode.getCode())
      .title(errorCode.getTitle())
      .message(errorCode.getMessage())
      .objects(errorObjects)
      .build();

    return handleExceptionInternal(ex, body, headers, errorCode.getStatus(), request);
  }

  private ApiError.ApiErrorObject getApiErrorObject(final ObjectError objectError) {
    var errorsList = Objects.requireNonNull(objectError.getArguments());
    var objectName = objectError instanceof FieldError ? ((FieldError) objectError).getField() : objectError.getObjectName();
    var objectMessage = messageSource.getMessage(objectError, Locale.ENGLISH);
    Integer objectCode = null;

    for (Object error : errorsList) {
      if (error instanceof ErrorCode) {
        var errorCode = ((ErrorCode) error);
        objectCode = errorCode.getCode();

        if (Objects.nonNull(errorCode.getMessage())) {
          objectMessage = errorCode.getMessage();
        }
        break;
      }
    }

    return ApiError.ApiErrorObject.builder().errorCode(objectCode).name(objectName).message(objectMessage).build();
  }

  private ApiError getBodyFromErrorCode(ErrorCode errorCode, String errorMessage) {
    return ApiError.builder()
      .errorCode(errorCode.getCode())
      .title(errorCode.getTitle())
      .message(errorMessage != null ? errorMessage : errorCode.getMessage())
      .build();
  }

  private void logError(ApiError body) {
    StringBuffer url = request.getRequestURL();
    String query = request.getQueryString();
    String logMessage = body.getMessage() != null ? body.getMessage() : body.getTitle();

    if (StringUtils.hasLength(query)) {
      url.append('?').append(query);
    }

    log.warn(FAIL_LOG_TEMPLATE, logMessage, url.toString(), request.getRemoteAddr());
  }

  private void logUnexpectedError(Exception ex) {
    StringBuffer url = request.getRequestURL();
    String query = request.getQueryString();

    if (StringUtils.hasLength(query)) {
      url.append('?').append(query);
    }

    log.error(FAIL_LOG_TEMPLATE, ErrorCode.INTERNAL_SERVER_ERROR.getTitle(), url.toString(), request.getRemoteAddr(), ex);
  }

  private void logValidationError(ApiError apiError) {
    StringBuffer url = request.getRequestURL();
    String query = request.getQueryString();

    if (StringUtils.hasLength(query)) {
      url.append('?').append(query);
    }

    var reasons = apiError.getObjects().stream()
      .map(ApiError.ApiErrorObject::getMessage)
      .collect(Collectors.joining(","));

    log.warn(FAIL_LOG_TEMPLATE, reasons, url.toString(), request.getRemoteAddr());
  }

  protected String getPropertyPath(List<Reference> references) {
    return references.stream().map(Reference::getFieldName).collect(Collectors.joining("."));
  }
}
