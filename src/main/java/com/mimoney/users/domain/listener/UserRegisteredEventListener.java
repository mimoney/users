package com.mimoney.users.domain.listener;

import com.mimoney.users.domain.event.UserRegisteredEvent;
import com.mimoney.users.domain.service.INotificationService;
import com.mimoney.users.infrastructure.messaging.dto.UserRegisteredDto;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@AllArgsConstructor
public class UserRegisteredEventListener implements ApplicationListener<UserRegisteredEvent> {

  private final INotificationService notificationService;

  @Override
  @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
  public void onApplicationEvent(UserRegisteredEvent event) {
    var token = event.getToken();
    notificationService.sendUserRegistryNotification(UserRegisteredDto.builder()
      .firstName(token.getUser().getFirstName())
      .lastName(token.getUser().getLastName())
      .userCode(token.getUser().getUserCode())
      .email(token.getUser().getEmail())
      .token(token.getTokenCode())
      .build());
  }

}
