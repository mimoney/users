package com.mimoney.users.api.v1.model.mapper;

import com.mimoney.users.api.v1.model.response.UserDetailResponseV1;
import com.mimoney.users.domain.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserDetailResponseV1Mapper extends BaseResponseV1Mapper<User, UserDetailResponseV1>{

  public UserDetailResponseV1Mapper(final ModelMapper mapper) {
    super(mapper);
  }
}
