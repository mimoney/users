package com.mimoney.users.domain.listener;

import com.mimoney.users.domain.event.PasswordResetRequestedEvent;
import com.mimoney.users.domain.service.INotificationService;
import com.mimoney.users.infrastructure.messaging.dto.PasswordResetDto;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@AllArgsConstructor
public class PasswordResetRequestedEventListener implements ApplicationListener<PasswordResetRequestedEvent> {

  private final INotificationService notificationService;

  @Override
  @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
  public void onApplicationEvent(PasswordResetRequestedEvent event) {
    var token = event.getToken();
    notificationService.sendPasswordResetNotification(PasswordResetDto.builder()
      .firstName(token.getUser().getFirstName())
      .lastName(token.getUser().getLastName())
      .userCode(token.getUser().getUserCode())
      .email(token.getUser().getEmail())
      .token(token.getTokenCode())
      .build());
  }
}
