package com.mimoney.users.domain.repository;

import com.mimoney.users.domain.model.GroupInvite;
import com.mimoney.users.domain.model.GroupInviteId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IGroupInviteRepository extends JpaRepository<GroupInvite, GroupInviteId> {

  Optional<GroupInvite> findGroupInviteByInviteCode(String inviteCode);

}
