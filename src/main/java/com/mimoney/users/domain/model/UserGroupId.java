package com.mimoney.users.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@Embeddable
@NoArgsConstructor
public class UserGroupId implements Serializable {

  @Column(name = "user_id")
  private Long userId;

  @EqualsAndHashCode.Include
  @Column(name = "user_group_id")
  private Long groupId;

  public UserGroupId(Long userId, Long groupId) {
    this.userId = userId;
    this.groupId = groupId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof UserGroupId)) return false;
    UserGroupId that = (UserGroupId) o;
    return userId.equals(that.userId) && groupId.equals(that.groupId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, groupId);
  }
}
