package com.mimoney.users.api.v1.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class GroupRequestV1 {

  @Schema(example = "Home Expenses")
  @NotBlank
  private String name;

  @Schema(example = "Group to share home expenses with family.")
  @NotBlank
  private String description;

}
