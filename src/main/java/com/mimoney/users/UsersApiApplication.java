package com.mimoney.users;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

@Slf4j
@SpringBootApplication(scanBasePackages = {"com.mimoney.users"})
public class UsersApiApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(UsersApiApplication.class);
		addDefaultProfile(app);
		setLocaleAndTimeZone();
		Environment env = app.run(args).getEnvironment();
		loggingAppStartup(env);
	}

	public static void setLocaleAndTimeZone() {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    Locale.setDefault(Locale.ENGLISH);
  }

	public static void addDefaultProfile(SpringApplication app) {
    Map<String, Object> props = new HashMap<>();
    props.put("spring.profiles.default", "dev");
    app.setDefaultProperties(props);
  }

  private static void loggingAppStartup(Environment env) {
	  String protocol = "http";
	  if (env.getProperty("server.ssl.key-store") != null) {
	    protocol = "https";
    }

    String serverPort = env.getProperty("server.port");
    String contextPath = env.getProperty("server.servlet.context-path");
    if (!StringUtils.hasText(contextPath)) {
      contextPath = "/";
    }

    String hostAddress = "localhost";
    try {
      hostAddress = InetAddress.getLocalHost().getHostAddress();
    } catch (UnknownHostException e) {
      log.warn("The host name could not be determined, using 'localhost' as fallback");
    }

    String appName = env.getProperty("spring.application.name");
    String activeProfile = env.getActiveProfiles().length > 0 ? env.getActiveProfiles()[1] : env.getProperty("spring.profiles.default");

    log.info("\n----------------------------------------------------------\n\t" +
        "Application '{}' is running! Access URLs:\n\t" +
        "Local: \t\t{}://localhost:{}{}\n\t" +
        "External: \t{}://{}:{}{}\n\t" +
        "Profile(s): \t{}" +
        "\n----------------------------------------------------------",
      appName,
      protocol,
      serverPort,
      contextPath,
      protocol,
      hostAddress,
      serverPort,
      contextPath,
      activeProfile);

    String configServerStatus = env.getProperty("configserver.status");
    if (configServerStatus == null) {
      configServerStatus = "Not found or not setup for this application";
    }
    log.info("\n----------------------------------------------------------\n\t" +
      "Config Server: \t{}\n----------------------------------------------------------", configServerStatus);

  }

}
