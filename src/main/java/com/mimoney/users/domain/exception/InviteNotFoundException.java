package com.mimoney.users.domain.exception;

import com.mimoney.users.domain.enumerated.ErrorCode;

public class InviteNotFoundException extends EntityNotFoundException {

  public InviteNotFoundException(String inviteCode) {
    super(ErrorCode.INVITE_NOT_FOUND, inviteCode);
  }
}
