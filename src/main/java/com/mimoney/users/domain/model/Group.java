package com.mimoney.users.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
@Table(name = "user_group")
@NoArgsConstructor
@AllArgsConstructor
public class Group {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "description", nullable = false)
  private String description;

  @OneToMany(mappedBy = "group", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private Set<UserGroup> users = new HashSet<>();

  public boolean isAdmin(User user) {
    return getUsers().stream().anyMatch(userGroup ->
      userGroup.getId().getUserId().equals(user.getId()) &&
      userGroup.isAdmin());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Group)) return false;
    Group group = (Group) o;
    return id.equals(group.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
