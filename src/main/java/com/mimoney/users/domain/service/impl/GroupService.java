package com.mimoney.users.domain.service.impl;

import com.mimoney.users.core.commons.Constants;
import com.mimoney.users.domain.enumerated.ErrorCode;
import com.mimoney.users.domain.event.UserInvitedEvent;
import com.mimoney.users.domain.exception.BusinessRuleException;
import com.mimoney.users.domain.exception.GroupNotFoundException;
import com.mimoney.users.domain.exception.InviteNotFoundException;
import com.mimoney.users.domain.model.Group;
import com.mimoney.users.domain.model.GroupInvite;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.model.UserGroup;
import com.mimoney.users.domain.repository.IGroupInviteRepository;
import com.mimoney.users.domain.repository.IGroupRepository;
import com.mimoney.users.domain.service.IGroupService;
import com.mimoney.users.domain.service.IUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class GroupService implements IGroupService {

  private final ApplicationEventPublisher eventPublisher;
  private final IUserService userService;
  private final IGroupRepository groupRepository;
  private final IGroupInviteRepository groupInviteRepository;

  @Override
  public List<Group> getUserGroups(String userCode) {
    var user = userService.getUserByCode(userCode);
    return groupRepository.findAllByUser(user.getId());
  }

  @Override
  public Group getGroupById(Long groupId) {
    return groupRepository.findById(groupId).orElseThrow(() -> new GroupNotFoundException(groupId.toString()));
  }

  @Override
  public Group createGroup(User user, Group group) {
    user.getGroups().add(new UserGroup(user, group, Boolean.TRUE));
    log.info("[{}] [Creating new group] [User: {}]", Constants.APP, user.getId());
    return groupRepository.save(group);
  }

  @Override
  public Group updateGroup(User user, Group group) {
    if (group.isAdmin(user)) {
      log.info("[{}] [Updating group] [User: {}] [Group: {}]", Constants.APP, user.getId(), group.getId());
      return groupRepository.save(group);
    } else {
      throw new BusinessRuleException(ErrorCode.ACCESS_DENIED);
    }
  }

  @Override
  public void leaveGroup(User user, Group group) {
    log.info("[{}] [Leaving group] [User: {}] [Group: {}]", Constants.APP, user.getId(), group.getId());
    user.getGroups().stream().filter(userGroup -> userGroup.getGroup().equals(group))
      .findAny().ifPresent(userGroup -> user.getGroups().remove(userGroup));
  }

  @Override
  public void inviteToGroup(User host, Group group, Set<String> guests) {
    var guestList = userService.getUsersByCodes(guests);
    guestList.removeIf(guest -> guest.equals(host));
    var invites = guestList.stream().map(guest -> new GroupInvite(host, guest, group)).collect(Collectors.toList());

    invites.forEach(invite -> {
      if (!groupInviteRepository.existsById(invite.getId())) {
        log.info("[{}] [Saving invite] [Host: {}] [Guest: {}] [Group: {}]", Constants.APP, invite.getHost().getId(), invite.getGuest().getId(), invite.getGroup().getId());
        groupInviteRepository.save(invite);
        eventPublisher.publishEvent(new UserInvitedEvent(invite));
      } else {
        log.warn("[{}] [Ignoring invite: Already exists] [Host: {}] [Guest: {}] [Group: {}]", Constants.APP, invite.getHost().getId(), invite.getGuest().getId(), invite.getGroup().getId());
      }
    });
  }

  @Override
  public void acceptInvite(User guest, String inviteCode) {
    var invite = getGroupInvite(inviteCode);

    if (guest.equals(invite.getGuest())) {
      log.info("[{}] [Accepting group invite] [Guest: {}] [Group: {}] [Invite: {}]", Constants.APP, guest.getId(), invite.getGroup().getId(), inviteCode);
      guest.getGroups().add(new UserGroup(guest, invite.getGroup(), Boolean.FALSE));
      groupInviteRepository.delete(invite);
    } else {
      throw new BusinessRuleException(ErrorCode.ACCESS_DENIED);
    }
  }

  @Override
  public void removeInvite(User user, String inviteCode) {
    var invite = getGroupInvite(inviteCode);

    if (user.equals(invite.getHost()) || user.equals(invite.getGuest())) {
      log.info("[{}] [Removing group invite] [User: {}] [Invite: {}]", Constants.APP, user.getId(), inviteCode);
      groupInviteRepository.delete(invite);
    } else {
      throw new BusinessRuleException(ErrorCode.ACCESS_DENIED);
    }
  }

  protected GroupInvite getGroupInvite(String inviteCode) {
    return groupInviteRepository.findGroupInviteByInviteCode(inviteCode).orElseThrow(() -> new InviteNotFoundException(inviteCode));
  }
}
