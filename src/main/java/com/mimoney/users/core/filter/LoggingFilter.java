package com.mimoney.users.core.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.stream.Collectors;

@Slf4j
@Component
public class LoggingFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    log.info("Initializing logging filter...");
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    long startTime = System.currentTimeMillis();
    HttpServletRequest httpServletRequest = null;
    boolean isHealthCheck = false;
    try {
      httpServletRequest = (HttpServletRequest) request;
      isHealthCheck = isHealthCheck(httpServletRequest);
      if (isHealthCheck) {
        log.debug("[IP] [{}] [Method] [{}] [Req] [{}{}] [Headers] [{}]",
          httpServletRequest.getRemoteAddr(),
          httpServletRequest.getMethod(),
          httpServletRequest.getPathInfo(),
          httpServletRequest.getQueryString(),
          formatHeaders(httpServletRequest));
      }
      chain.doFilter(request, response);
    } finally {
      if (!isHealthCheck) {
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        log.info("[RespTime] [{}] [Status] [{}] [IP] [{}] [Req] [{}]",
          (System.currentTimeMillis() - startTime),
          HttpStatus.valueOf(httpServletResponse.getStatus()),
          request.getRemoteAddr(),
          getQuery(httpServletRequest));
      }
    }
  }

  @Override
  public void destroy() {
    log.info("Destroying logging filter...");
  }

  private boolean isHealthCheck(HttpServletRequest http) {
    return http.getRequestURL().toString().endsWith("/ping")
      || http.getRequestURL().toString().endsWith("/health")
      || http.getRequestURL().toString().contains("/health/");
  }

  private String formatHeaders(HttpServletRequest http) {
    return Collections.list(http.getHeaderNames())
      .stream()
      .map(item -> String.format(",%s=%s", item, http.getHeader(item)))
      .collect(Collectors.joining());
  }

  private String getQuery(HttpServletRequest http) {
    String query = http.getRequestURL().toString();
    if (http.getQueryString() != null) {
      query = query + "?" + http.getQueryString();
    }
    return query;
  }
}
