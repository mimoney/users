package com.mimoney.users.api.v1.controller;

import com.mimoney.users.UsersApiApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.mimoney.users.utils.TestUtils.getResource;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = UsersApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerV1IntegrationTest extends AbstractIntegrationTest {

  private MockMvc mvc;

  @BeforeEach
  public void setUp() {
    mvc = setupMvc();
    runTestData();
  }

  @Test
  public void shouldReturn200_whenGetUserList() throws Exception {
    mvc.perform(
      get(_V1_USERS)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/users/user-list-response.json")));
  }

  @Test
  public void shouldReturn200_whenGetUserByCode() throws Exception {
    mvc.perform(
      get(_V1_USERS + _USER_CODE, VALID_USER_CODE)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/users/user-detail-response.json")));
  }

  @Test
  public void shouldReturn200_whenUpdateUser() throws Exception {
    mvc.perform(
      put(_V1_USERS + _USER_CODE, VALID_USER_CODE)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(getResource("data/request/v1/users/user-update-body.json"))
    ).andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/users/updated-user-detail-response.json")));
  }

  @Test
  public void shouldReturn404_givenInvalidCode() throws Exception {
    mvc.perform(
      get(_V1_USERS + _USER_CODE, INVALID_USER_CODE)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isNotFound())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/error-response/user-not-found.json")));
  }

  @Test
  public void shouldReturn406_givenInvalidAcceptHeader() throws Exception {
    mvc.perform(get(_V1_USERS).accept(MediaType.APPLICATION_XML)).andExpect(status().isNotAcceptable());
  }

}
