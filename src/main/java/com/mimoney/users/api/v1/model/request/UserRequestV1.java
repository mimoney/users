package com.mimoney.users.api.v1.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Getter
@Setter
public class UserRequestV1 {

  @Schema(example = ParamExample.FIRST_NAME)
  @NotBlank
  private String firstName;

  @Schema(example = ParamExample.LAST_NAME)
  @NotBlank
  private String lastName;

}
