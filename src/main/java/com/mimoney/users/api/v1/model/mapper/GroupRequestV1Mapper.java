package com.mimoney.users.api.v1.model.mapper;

import com.mimoney.users.api.v1.model.request.GroupRequestV1;
import com.mimoney.users.domain.model.Group;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class GroupRequestV1Mapper extends BaseRequestV1Mapper<GroupRequestV1, Group> {

  public GroupRequestV1Mapper(final ModelMapper mapper) {
    super(mapper);
  }
}
