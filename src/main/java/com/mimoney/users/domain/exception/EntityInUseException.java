package com.mimoney.users.domain.exception;

import com.mimoney.users.domain.enumerated.ErrorCode;
import lombok.Getter;

@Getter
public class EntityInUseException extends RuntimeException {

  private final ErrorCode errorCode;

  public EntityInUseException(ErrorCode errorCode) {
    super(errorCode.getMessage());
    this.errorCode = errorCode;
  }
}
