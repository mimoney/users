package com.mimoney.users.api.validation;

import com.mimoney.users.api.validation.validator.PasswordValidator;
import com.mimoney.users.domain.enumerated.ErrorCode;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Rodrigo Bacchetti
 * This validator verifies if the given string matches the MiMoney password requirements:
 * - Its length must be between 8 and 30 characters.
 * - It must have at least 1 uppercase character. (e.g. 'ABC')
 * - It must have at least 1 digit (number) character. (e.g. '123')
 * - It must have at least 1 special character (e.g. '@#$%')
 */
@Documented
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
public @interface ValidPassword {

  String message() default "";

  ErrorCode error() default ErrorCode.INVALID_PASSWORD;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
