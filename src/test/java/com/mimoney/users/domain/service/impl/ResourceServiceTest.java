package com.mimoney.users.domain.service.impl;

import com.mimoney.users.domain.exception.ResourceNotFoundException;
import com.mimoney.users.domain.model.Resource;
import com.mimoney.users.domain.model.ResourceId;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.repository.IResourceRepository;
import com.mimoney.users.domain.service.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ResourceServiceTest {

  private static final String USER_CODE = "USER_CODE";
  private static final Long RESOURCE_ID = 1L;

  private ResourceService fixture;

  @Mock
  private IUserService userService;
  @Mock
  private IResourceRepository resourceRepository;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    fixture = new ResourceService(userService, resourceRepository);
  }

  @Test
  public void shouldGetResourcesByUser() {
    when(userService.getUserByCode(USER_CODE)).thenReturn(mock(User.class));
    var mockList = Arrays.asList(new Resource(), new Resource());
    when(resourceRepository.findByUser(any(User.class))).thenReturn(mockList);

    var result = fixture.getUserResources(USER_CODE);

    verify(userService).getUserByCode(anyString());
    verify(resourceRepository).findByUser(any(User.class));
    assertEquals(2, result.size());
  }

  @Test
  public void shouldGetResourceByID() {
    when(userService.getUserByCode(USER_CODE)).thenReturn(mock(User.class));
    when(resourceRepository.findById(any(ResourceId.class))).thenReturn(Optional.of(new Resource()));

    var result = fixture.getUserResourceById(USER_CODE, RESOURCE_ID);

    verify(userService).getUserByCode(anyString());
    verify(resourceRepository).findById(any(ResourceId.class));
    assertNotNull(result);
  }

  @Test
  public void shouldCreateResource() {
    when(userService.getUserByCode(USER_CODE)).thenReturn(mock(User.class));
    when(resourceRepository.save(any(Resource.class))).thenReturn(new Resource());

    var result = fixture.addUserResource(USER_CODE, mock(Resource.class));

    verify(userService).getUserByCode(anyString());
    verify(resourceRepository).save(any(Resource.class));
    assertNotNull(result);
  }

  @Test
  public void shouldUpdateResource() {
    when(resourceRepository.save(any(Resource.class))).thenReturn(new Resource());

    var mockResource = mock(Resource.class, RETURNS_DEEP_STUBS);
    var result = fixture.updateUserResource(mockResource);

    verify(resourceRepository).save(any(Resource.class));
    assertNotNull(result);
  }

  @Test
  public void shouldRemoveResource() {
    var mockResource = mock(Resource.class, RETURNS_DEEP_STUBS);
    when(userService.getUserByCode(USER_CODE)).thenReturn(mock(User.class));
    when(resourceRepository.findById(any(ResourceId.class))).thenReturn(Optional.of(mockResource));

    fixture.removeUserResource(USER_CODE, RESOURCE_ID);

    verify(resourceRepository).delete(any(Resource.class));
  }

  @Test
  public void shouldThrowResourceNotFoundException() {
    when(userService.getUserByCode(USER_CODE)).thenReturn(mock(User.class));
    when(resourceRepository.findById(any(ResourceId.class))).thenReturn(Optional.empty());

    assertThrows(ResourceNotFoundException.class, () -> fixture.getUserResourceById(USER_CODE, RESOURCE_ID));
  }

}
