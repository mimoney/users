package com.mimoney.users.api.v1.controller;

import com.mimoney.users.api.v1.model.mapper.UserDetailResponseV1Mapper;
import com.mimoney.users.api.v1.model.mapper.UserRegisterRequestV1Mapper;
import com.mimoney.users.api.v1.model.request.PasswordRequestV1;
import com.mimoney.users.api.v1.model.request.UserRegisterRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.api.v1.model.request.parameter.UserTokenParam;
import com.mimoney.users.api.v1.model.response.UserDetailResponseV1;
import com.mimoney.users.api.v1.openapi.controller.UserAccessControllerV1OpenApi;
import com.mimoney.users.domain.service.IUserAccessService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.mimoney.users.core.commons.Constants.USERS;
import static com.mimoney.users.core.commons.Constants.V1;

@AllArgsConstructor
@RestController
@RequestMapping(path = V1 + USERS, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserAccessControllerV1 implements UserAccessControllerV1OpenApi {

  private final IUserAccessService userAccessService;
  private final UserDetailResponseV1Mapper userDetailResponseV1Mapper;
  private final UserRegisterRequestV1Mapper userRegisterRequestV1Mapper;

  @PostMapping("/registration")
  @ResponseStatus(HttpStatus.CREATED)
  public UserDetailResponseV1 registerNewUser(@Valid @RequestBody UserRegisterRequestV1 body) {
    var newUser = userAccessService.registerNewUser(userRegisterRequestV1Mapper.map(body));
    return userDetailResponseV1Mapper.map(newUser);
  }

  @GetMapping("/{userCode}/confirmRegistration/{token}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void confirmUserRegistration(@Valid UserTokenParam param) {
    userAccessService.confirmRegistration(param.getUserCode(), param.getToken());
  }

  @GetMapping("/requestPassword")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void requestPasswordReset(@Valid @RequestParam(name = "email") String email) {
    userAccessService.requestPasswordReset(email);
  }

  @PutMapping("/{userCode}/resetPassword/{token}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void passwordReset(@Valid UserTokenParam param, @RequestBody PasswordRequestV1 password) {
    userAccessService.resetPassword(param.getUserCode(), param.getToken(), password.getPassword());
  }

  @PutMapping("/{userCode}/activation")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void activateUser(@Valid UserParam param) {
    userAccessService.activateUser(param.getUserCode());
  }

  @DeleteMapping("/{userCode}/deactivation")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deactivateUser(@Valid UserParam param) {
    userAccessService.deactivateUser(param.getUserCode());
  }

}
