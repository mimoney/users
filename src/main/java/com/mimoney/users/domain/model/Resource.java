package com.mimoney.users.domain.model;

import com.mimoney.users.domain.enumerated.ResourceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Data
@Entity
@Table(name = "resource")
@NoArgsConstructor
@AllArgsConstructor
public class Resource {

  @EmbeddedId
  private ResourceId id;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "value")
  private BigDecimal value;

  @Enumerated(EnumType.STRING)
  @Column(name = "type", nullable = false)
  private ResourceType type;

  @ManyToOne
  @JoinColumn(name = "user_id", nullable = false, insertable = false, updatable = false)
  private User user;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Resource)) return false;
    Resource resource = (Resource) o;
    return id.equals(resource.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
