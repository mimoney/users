package com.mimoney.users.api.validation;

import com.mimoney.users.api.validation.validator.UserCodeListValidator;
import com.mimoney.users.domain.enumerated.ErrorCode;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = UserCodeListValidator.class)
public @interface ValidUserCodeList {

  String message() default "";

  boolean nullable() default false;

  ErrorCode error() default ErrorCode.INVALID_USER_CODE;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
