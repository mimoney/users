package com.mimoney.users.api.v1.openapi.response.error;

import com.mimoney.users.api.exception.ApiError;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

@Target({METHOD, TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@ApiResponse(
  responseCode = "403",
  description = "Access Denied - The user does not have the required permissions.",
  content =
  @Content(
    mediaType = MediaType.APPLICATION_JSON_VALUE,
    schema = @Schema(implementation = ApiError.class),
    examples = {
      @ExampleObject(
        "{\n" +
          "  \"errorCode\": 9992,\n" +
          "  \"title\": \"Access Denied\",\n" +
          "  \"message\": \"You do not have permission to execute this action.\"\n" +
          "}\n")
    })
)
public @interface AccessDenied403Response {
}
