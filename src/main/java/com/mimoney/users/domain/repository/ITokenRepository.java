package com.mimoney.users.domain.repository;

import com.mimoney.users.domain.enumerated.TokenType;
import com.mimoney.users.domain.model.Token;
import com.mimoney.users.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ITokenRepository extends JpaRepository<Token, String> {

  @Query("from Token t where t.tokenCode = :token and t.type = :type and t.user = :user")
  Optional<Token> getToken(String token, TokenType type, User user);

}
