package com.mimoney.users.api.v1.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Getter
@Setter
public class UserShortResponseV1 {

  @Schema(example = ParamExample.USER_CODE)
  private String code;

  @Schema(example = ParamExample.USER_NAME)
  private String userName;

  @Schema(example = ParamExample.FIRST_NAME)
  private String firstName;

  @Schema(example = ParamExample.LAST_NAME)
  private String lastName;

  @Schema(example = ParamExample.EMAIL)
  private String email;

  @Schema(example = "true")
  private boolean admin;

}
