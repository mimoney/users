package com.mimoney.users.api.v1.openapi.controller;

import com.mimoney.users.api.v1.model.request.PasswordRequestV1;
import com.mimoney.users.api.v1.model.request.UserRegisterRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.api.v1.model.request.parameter.UserTokenParam;
import com.mimoney.users.api.v1.model.response.UserDetailResponseV1;
import com.mimoney.users.api.v1.openapi.OpenApiConstants;
import com.mimoney.users.api.v1.openapi.response.Created201Response;
import com.mimoney.users.api.v1.openapi.response.NoContent204Response;
import com.mimoney.users.api.v1.openapi.response.error.GlobalErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamDescription;
import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Tag(name = OpenApiConstants.Tag.TAG_USERS_ACCESS, description = "User access  management operations.")
public interface UserAccessControllerV1OpenApi {

  @Created201Response
  @GlobalErrorResponse
  @Operation(summary = "Register new user", description = "Registers a new user.")
  UserDetailResponseV1 registerNewUser(@RequestBody UserRegisterRequestV1 body);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Confirm user registration", description = "Confirms and activates a user by its user code and registration token.")
  void confirmUserRegistration(@ParameterObject UserTokenParam param);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Request password reset", description = "Requests a user password reset by its email.")
  void requestPasswordReset(@Parameter(in = ParameterIn.QUERY, description = ParamDescription.EMAIL, example = ParamExample.EMAIL) String email);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Reset password", description = "Reset a user password by its user code and password-reset token.")
  void passwordReset(@ParameterObject UserTokenParam param, @RequestBody PasswordRequestV1 password);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Activate user", description = "Activates a user by its user code.")
  void activateUser(@ParameterObject UserParam param);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Deactivate user", description = "Deactivates a user by its user code.")
  void deactivateUser(@ParameterObject UserParam param);

}
