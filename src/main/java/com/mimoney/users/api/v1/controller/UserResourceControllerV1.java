package com.mimoney.users.api.v1.controller;

import com.mimoney.users.api.v1.model.mapper.ResourceRequestV1Mapper;
import com.mimoney.users.api.v1.model.mapper.ResourceResponseV1Mapper;
import com.mimoney.users.api.v1.model.request.ResourceRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.ResourceParam;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.api.v1.model.response.ResourceResponseV1;
import com.mimoney.users.api.v1.openapi.controller.UserResourceControllerV1OpenApi;
import com.mimoney.users.domain.service.IResourceService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.mimoney.users.core.commons.Constants.RESOURCES;
import static com.mimoney.users.core.commons.Constants.V1;

@AllArgsConstructor
@RestController
@RequestMapping(path = V1 + RESOURCES, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserResourceControllerV1 implements UserResourceControllerV1OpenApi {

  private final IResourceService resourceService;
  private final ResourceResponseV1Mapper resourceResponseMapper;
  private final ResourceRequestV1Mapper resourceRequestMapper;

  @GetMapping
  public List<ResourceResponseV1> getUserResources(@Valid UserParam param) {
    return resourceResponseMapper.map(resourceService.getUserResources(param.getUserCode()));
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResourceResponseV1 addUserResource(@Valid UserParam param, @Valid @RequestBody ResourceRequestV1 body) {
    var resource = resourceRequestMapper.map(body);
    return resourceResponseMapper.map(resourceService.addUserResource(param.getUserCode(), resource));
  }

  @PutMapping("/{resourceId}")
  public ResourceResponseV1 updateUserResource(@Valid ResourceParam param, @Valid @RequestBody ResourceRequestV1 body) {
    var resource = resourceService.getUserResourceById(param.getUserCode(), param.getResourceId());
    resourceRequestMapper.copy(body, resource);
    return resourceResponseMapper.map(resourceService.updateUserResource(resource));
  }

  @DeleteMapping("/{resourceId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void removeUserResource(@Valid ResourceParam param) {
    resourceService.removeUserResource(param.getUserCode(), param.getResourceId());
  }
}
