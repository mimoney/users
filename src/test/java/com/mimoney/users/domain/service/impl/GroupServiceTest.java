package com.mimoney.users.domain.service.impl;

import com.mimoney.users.domain.exception.BusinessRuleException;
import com.mimoney.users.domain.exception.GroupNotFoundException;
import com.mimoney.users.domain.exception.InviteNotFoundException;
import com.mimoney.users.domain.model.*;
import com.mimoney.users.domain.repository.IGroupInviteRepository;
import com.mimoney.users.domain.repository.IGroupRepository;
import com.mimoney.users.domain.service.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GroupServiceTest {

  private static final String CODE_1 = "1";
  private static final String CODE_2 = "2";
  private static final String CODE_3 = "3";
  private static final String USER_CODE = "USER_CODE";
  private static final Long GROUP_ID = 1L;
  private User user;
  private Group group;

  private GroupService fixture;

  @Mock
  private IUserService userService;
  @Mock
  private IGroupRepository groupRepository;
  @Mock
  private IGroupInviteRepository groupInviteRepository;
  @Mock
  private ApplicationEventPublisher publisher;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    fixture = new GroupService(publisher, userService, groupRepository, groupInviteRepository);
    lenient().doNothing().when(publisher).publishEvent(any());
  }

  @Test
  public void shouldGetGroupsByUser() {
    var mockList = Arrays.asList(mock(Group.class), mock(Group.class));
    when(groupRepository.findAllByUser(anyLong())).thenReturn(mockList);
    when(userService.getUserByCode(USER_CODE)).thenReturn(mock(User.class));

    var result = fixture.getUserGroups(USER_CODE);

    verify(userService).getUserByCode(anyString());
    verify(groupRepository).findAllByUser(anyLong());
    assertEquals(2, result.size());
  }

  @Test
  public void shouldGetGroupByID() {
    when(groupRepository.findById(GROUP_ID)).thenReturn(Optional.of(new Group()));

    var result = fixture.getGroupById(GROUP_ID);

    verify(groupRepository).findById(GROUP_ID);
    assertNotNull(result);
  }

  @Test
  public void shouldCreateGroup() {
    when(groupRepository.save(any())).thenReturn(mock(Group.class));

    var result = fixture.createGroup(new User(), new Group());

    verify(groupRepository).save(any());
    assertNotNull(result);
  }

  @Test
  public void shouldUpdateGroupIfAdmin() {
    setUpUserGroup(1L, 1L, true);
    when(groupRepository.save(group)).thenReturn(group);

    var result = fixture.updateGroup(user, group);

    verify(groupRepository).save(any());
    assertNotNull(result);
  }

  @Test
  public void shouldLeaveGroup() {
    setUpUserGroup(1L,1L,false);
    assertTrue(user.isMemberOf(group));

    fixture.leaveGroup(user, group);

    assertFalse(user.isMemberOf(group));
  }

  @Test
  public void shouldInvite() {
    setUpUserGroup(1L,1L,true);
    user = mockUser("4");
    var guests = Set.of(CODE_1, CODE_2, CODE_3);
    var guestList = mockUsers();

    when(userService.getUsersByCodes(guests)).thenReturn(guestList);
    when(groupInviteRepository.existsById(any(GroupInviteId.class))).thenReturn(false);

    fixture.inviteToGroup(user, group, guests);

    verify(userService).getUsersByCodes(anySet());
    verify(groupInviteRepository, times(3)).existsById(any(GroupInviteId.class));
    verify(groupInviteRepository, times(3)).save(any(GroupInvite.class));
  }

  @Test
  public void shouldAcceptInvite() {
    user = mockUser("2");
    var invite = new GroupInvite(mockUser("1"), user, mock(Group.class));
    when(groupInviteRepository.findGroupInviteByInviteCode(anyString())).thenReturn(Optional.of(invite));

    fixture.acceptInvite(user, "INVITE_CODE");

    assertEquals(1, user.getGroups().size());
    verify(groupInviteRepository).delete(any(GroupInvite.class));
  }
  @Test
  public void shouldRemoveInvite() {
    var host = mockUser("1");
    var guest = mockUser("2");
    var invite = new GroupInvite(host, guest, mock(Group.class));
    when(groupInviteRepository.findGroupInviteByInviteCode(anyString())).thenReturn(Optional.of(invite));

    fixture.removeInvite(host, "INVITE");
    fixture.removeInvite(guest, "INVITE");
    verify(groupInviteRepository, times(2)).delete(any(GroupInvite.class));
  }

  @Test
  public void shouldThrowBusinessException_whenNotInviteGuest() {
    user = mockUser("1");
    var invite = new GroupInvite(mockUser("2"), mockUser("3"), mock(Group.class));
    when(groupInviteRepository.findGroupInviteByInviteCode(anyString())).thenReturn(Optional.of(invite));

    assertThrows(BusinessRuleException.class, () -> fixture.acceptInvite(user, "INVITE_CODE"));
  }

  @Test
  public void shouldThrowBusinessException_whenNotInviteRelated() {
    user = mockUser("1");
    var invite = new GroupInvite(mockUser("2"), mockUser("3"), mock(Group.class));
    when(groupInviteRepository.findGroupInviteByInviteCode(anyString())).thenReturn(Optional.of(invite));

    assertThrows(BusinessRuleException.class, () -> fixture.removeInvite(user, "INVITE_CODE"));
  }

  @Test
  public void shouldNotSendInvite_whenAlreadyInvited() {
    setUpUserGroup(1L,1L,true);
    var guests = Set.of(CODE_1);
    var guestList = mockUsers();

    when(userService.getUsersByCodes(guests)).thenReturn(guestList);
    when(groupInviteRepository.existsById(any(GroupInviteId.class))).thenReturn(true);

    fixture.inviteToGroup(user, group, guests);

    verify(userService).getUsersByCodes(anySet());
    verify(groupInviteRepository, times(2)).existsById(any(GroupInviteId.class));
    verify(groupInviteRepository, never()).save(any(GroupInvite.class));
  }

//  @Test
//  public void shouldGetGroupInvite() {
//    when(groupInviteRepository.findGroupInviteByInviteCode(anyString())).thenReturn(Optional.of(new GroupInvite()));
//    assertNotNull(fixture.getGroupInvite(mock(String.class)));
//  }

  @Test
  public void shouldThrowInviteNotFoundException_whenGetInvalidGroupInvite() {
    when(groupInviteRepository.findGroupInviteByInviteCode(anyString())).thenReturn(Optional.empty());
    assertThrows(InviteNotFoundException.class, () -> fixture.getGroupInvite("INVITE"));
  }

  @Test
  public void shouldThrowBusinessRuleExceptionIfNonAdmin() {
    setUpUserGroup(1L,1L,false);
    assertThrows(BusinessRuleException.class, () -> fixture.updateGroup(user, group));
  }

  @Test
  public void shouldThrowGroupNotFoundException() {
    when(groupRepository.findById(GROUP_ID)).thenReturn(Optional.empty());
    assertThrows(GroupNotFoundException.class, () -> fixture.getGroupById(GROUP_ID));
  }

  private User mockUser(String code) {
    var user = new User();
    user.setId(Long.valueOf(code));
    user.setUserCode("CODE_" + code);
    return user;
  }

  private List<User> mockUsers() {
    List<User> users = new ArrayList<>();
    users.add(mockUser("1"));
    users.add(mockUser("2"));
    users.add(mockUser("3"));
    return users;
  }

  private void setUpUserGroup(Long userId, Long groupId, boolean admin) {
    user = new User();
    group = new Group();

    user.setId(userId);
    group.setId(groupId);
    var userGroup = new HashSet<UserGroup>();
    userGroup.add(new UserGroup(user, group, admin));

    user.setGroups(userGroup);
    group.setUsers(userGroup);
  }
}
