package com.mimoney.users.api.v1.model.mapper;

import com.mimoney.users.api.v1.model.response.GroupResponseV1;
import com.mimoney.users.domain.model.Group;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class GroupResponseV1Mapper extends BaseResponseV1Mapper<Group, GroupResponseV1>{

  public GroupResponseV1Mapper(final ModelMapper mapper) {
    super(mapper);
  }
}
