package com.mimoney.users.api.validation.validator;

import com.mimoney.users.api.validation.ValidPassword;
import org.passay.*;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Objects;

@Component
public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {

    if (Objects.isNull(value)) {
      return false;
    }

    var validator = new org.passay.PasswordValidator(Arrays.asList(
      new LengthRule(8, 30),
      new CharacterRule(EnglishCharacterData.UpperCase, 1),
      new CharacterRule(EnglishCharacterData.Digit, 1),
      new CharacterRule(EnglishCharacterData.Special, 1),
      new RepeatCharacterRegexRule(3)
    ));

    RuleResult result = validator.validate(new PasswordData(value));

    if (result.isValid()) {
      return true;
    } else {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(String.join("", validator.getMessages(result)).replace(".", "; "));
      return false;
    }
  }
}
