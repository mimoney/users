package com.mimoney.users.domain.exception;

import com.mimoney.users.domain.enumerated.ErrorCode;

public class TokenNotFoundException extends EntityNotFoundException {

  public TokenNotFoundException(String token) {
    super(ErrorCode.TOKEN_NOT_FOUND, token);
  }

}
