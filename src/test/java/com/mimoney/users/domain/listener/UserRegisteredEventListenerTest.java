package com.mimoney.users.domain.listener;

import com.mimoney.users.domain.enumerated.TokenType;
import com.mimoney.users.domain.event.UserRegisteredEvent;
import com.mimoney.users.domain.model.Token;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.service.INotificationService;
import com.mimoney.users.infrastructure.messaging.dto.UserRegisteredDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserRegisteredEventListenerTest {

  @InjectMocks
  private UserRegisteredEventListener fixture;
  @Mock
  private INotificationService notificationService;

  @Test
  void handleEvent() {
    fixture.onApplicationEvent(new UserRegisteredEvent(new Token(new User(), TokenType.REGISTRATION)));
    Mockito.verify(notificationService).sendUserRegistryNotification(Mockito.any(UserRegisteredDto.class));
  }
}
