package com.mimoney.users.api.v1.openapi.response.error;

import com.mimoney.users.api.exception.ApiError;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

@Target({METHOD, TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@ApiResponse(
  responseCode = "404",
  description = "Not Found - The requested api resource was not found.",
  content =
  @Content(
    mediaType = MediaType.APPLICATION_JSON_VALUE,
    schema = @Schema(implementation = ApiError.class),
    examples = {
      @ExampleObject(
        "{\n" +
          "  \"errorCode\": 3000,\n" +
          "  \"title\": \"Not Found\",\n" +
          "  \"message\": \"Api Resource '<CODE>' not found.\"\n" +
          "}\n")
    })
)
public @interface NotFound404ErrorResponse {
}
