package com.mimoney.users.api.v1.controller;


import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static com.mimoney.users.core.commons.Constants.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;

@Slf4j
@Testcontainers
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ContextConfiguration(initializers = AbstractIntegrationTest.ContainerInitializer.class)
public abstract class AbstractIntegrationTest {

  protected static final String _V1_USERS = V1 + USERS;
  protected static final String _V1_USERS_CODE_GROUPS = V1 + GROUPS;
  protected static final String _V1_USERS_CODE_RESOURCES = V1 + RESOURCES;
  protected static final String _GROUP_ID = "/{groupId}";
  protected static final String _USER_CODE = "/{userCode}";
  protected static final String _RESOURCE_ID = "/{resourceId}";
  protected static final String VALID_USER_CODE = "1a7152b3-6ec8-4966-8471-1766e3b996b6";
  protected static final String INVALID_USER_CODE = "INVALID_USER_CODE";

  @Autowired
  private WebApplicationContext webApplicationContext;

  protected MockMvc setupMvc() {
    return MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  private static final MySQLContainer<?> mysqlContainer = new MySQLContainer<>("mysql:8.0.25")
    .withDatabaseName("mimoney_users_test")
    .withUsername("root")
    .withPassword("")
    .withUrlParam("useSSL", "false");

  private static final KafkaContainer kafkaContainer =
    new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.4"));

  static {
    mysqlContainer.start();
    kafkaContainer.start();
  }

  protected void runTestData() {
    Flyway flyway = Flyway.configure()
      .dataSource(mysqlContainer.getJdbcUrl(), mysqlContainer.getUsername(), mysqlContainer.getPassword())
      .locations("classpath:db/testdata")
      .load();
    flyway.migrate();
  }

  public static class ContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
      TestPropertyValues.of(
        "spring.datasource.url=" + mysqlContainer.getJdbcUrl(),
        "spring.datasource.username=" + mysqlContainer.getUsername(),
        "spring.datasource.password=" + mysqlContainer.getPassword(),
        "environment.kafka.host=" + kafkaContainer.getHost(),
        "environment.kafka.port=" + kafkaContainer.getMappedPort(9093)
      ).applyTo(applicationContext.getEnvironment());
    }
  }
}
