package com.mimoney.users.api.validation.validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class PasswordValidatorTest {

  @InjectMocks
  private PasswordValidator fixture;

  @Mock
  private ConstraintValidatorContext context;

  @Test
  public void isValid() {
    assertTrue(fixture.isValid("Pa$$w0rd", context));
  }

  @Test
  public void isNotValid() {
    assertFalse(fixture.isValid(null, context));
    assertFalse(fixture.isValid("", context));
    assertFalse(fixture.isValid("password", context));
    assertFalse(fixture.isValid("PASSWORD", context));
    assertFalse(fixture.isValid("Pa$$$w0rd", context));
    assertFalse(fixture.isValid("12345", context));
  }
}
