package com.mimoney.users.api.v1.model.request;

import com.mimoney.users.api.validation.ValidPassword;
import com.mimoney.users.api.validation.ValidPasswordConfirmation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Getter
@Setter
@ValidPasswordConfirmation
public class PasswordRequestV1 {

  @Schema(example = ParamExample.PASS_EXAMPLE, required = true)
  @ValidPassword
  private String password;

  @Schema(example = ParamExample.PASS_EXAMPLE, required = true)
  private String confirmPassword;

}
