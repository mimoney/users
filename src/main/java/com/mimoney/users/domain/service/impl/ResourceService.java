package com.mimoney.users.domain.service.impl;

import com.mimoney.users.core.commons.Constants;
import com.mimoney.users.domain.exception.ResourceNotFoundException;
import com.mimoney.users.domain.model.Resource;
import com.mimoney.users.domain.model.ResourceId;
import com.mimoney.users.domain.repository.IResourceRepository;
import com.mimoney.users.domain.service.IResourceService;
import com.mimoney.users.domain.service.IUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class ResourceService implements IResourceService {

  private final IUserService userService;
  private final IResourceRepository resourceRepository;

  @Override
  public List<Resource> getUserResources(String userCode) {
    var user = userService.getUserByCode(userCode);
    return resourceRepository.findByUser(user);
  }

  @Override
  public Resource getUserResourceById(String userCode, Long resourceId) {
    var user = userService.getUserByCode(userCode);
    return resourceRepository.findById(new ResourceId(user.getId(), resourceId)).orElseThrow(
      () -> new ResourceNotFoundException(resourceId.toString(), userCode));
  }

  @Override
  public Resource addUserResource(String userCode, Resource resource) {
    var user = userService.getUserByCode(userCode);
    resource.setId(new ResourceId(user.getId(), resourceRepository.getNextId(user)));
    log.info("[{}] [Adding resource] [User: {}]", Constants.APP, user.getId());
    return resourceRepository.save(resource);
  }

  @Override
  public Resource updateUserResource(Resource resource) {
    log.info("[{}] [Updating resource] [User: {}] [Resource: {}]", Constants.APP, resource.getId().getUserId(), resource.getId().getUserId());
    return resourceRepository.save(resource);
  }

  @Override
  public void removeUserResource(String userCode, Long resourceId) {
    var resource = getUserResourceById(userCode, resourceId);
    log.info("[{}] [Removing resource] [User: {}] [Resource: {}]", Constants.APP, resource.getId().getUserId(), resource.getId().getUserId());
    resourceRepository.delete(resource);
  }
}
