package com.mimoney.users.domain.model;

import com.mimoney.users.domain.enumerated.TokenType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Objects;
import java.util.Random;

@Data
@NoArgsConstructor
@Entity
@Table(name = "token")
public class Token {

  @Id
  @Column(name = "token", nullable = false)
  private String tokenCode;

  @Enumerated(EnumType.STRING)
  @Column(name = "type")
  private TokenType type;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  @Column(name = "expiration_date", nullable = false)
  private OffsetDateTime expirationDate;

  public Token(User user, TokenType type) {
    this.tokenCode = String.format("%06d", new Random().nextInt(999999));
    this.expirationDate = OffsetDateTime.now().plusHours(48);
    this.user = user;
    this.type = type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Token)) return false;
    Token token = (Token) o;
    return tokenCode.equals(token.tokenCode) && type == token.type && user.equals(token.user);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tokenCode, type, user);
  }
}
