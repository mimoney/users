package com.mimoney.users.api.v1.controller;

import com.mimoney.users.UsersApiApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.mimoney.users.utils.TestUtils.getResource;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = UsersApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserGroupControllerV1IntegrationTest extends AbstractIntegrationTest {

  private MockMvc mvc;

  @BeforeEach
  public void setUp() {
    mvc = setupMvc();
    runTestData();
  }

  @Test
  public void shouldReturn200_whenGetGroupList() throws Exception {
    mvc.perform(
      get(_V1_USERS_CODE_GROUPS, VALID_USER_CODE)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/groups/group-list-response.json")));
  }

  @Test
  public void shouldReturn200_whenGetGroupById() throws Exception {
    mvc.perform(
      get(_V1_USERS_CODE_GROUPS + _GROUP_ID, VALID_USER_CODE, 1L)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/groups/group-detail-response.json")));
  }

  @Test
  public void shouldReturn201_whenCreateGroup() throws Exception {
    mvc.perform(
      post(_V1_USERS_CODE_GROUPS, VALID_USER_CODE)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(getResource("data/request/v1/groups/group-create-body.json"))
    ).andExpect(status().isCreated())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/groups/created-group-response.json")));
  }

  @Test
  public void shouldReturn200_whenAdminUpdatesGroup() throws Exception {
    mvc.perform(
      put(_V1_USERS_CODE_GROUPS + _GROUP_ID, VALID_USER_CODE, 1L)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(getResource("data/request/v1/groups/group-update-body.json"))
    ).andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/groups/updated-group-response.json")));
  }

  @Test
  public void shouldReturn204_whenLeaveGroup() throws Exception {
    mvc.perform(
      delete(_V1_USERS_CODE_GROUPS + _GROUP_ID, VALID_USER_CODE, 1L)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isNoContent());
  }

  @Test
  public void shouldReturn204_whenInviteToGroup() throws Exception {
    mvc.perform(
      get(_V1_USERS_CODE_GROUPS + _GROUP_ID + "/invite" +
        "?userCodes=29946286-0f78-41fa-84fa-d3278d2a97c4,1166150a-feee-45d4-b006-7942eef4f8d5",
        VALID_USER_CODE, 2L)
    ).andExpect(status().isNoContent());
  }

  @Test
  public void shouldReturn204_whenAcceptInvite() throws Exception {
    mvc.perform(
      put(_V1_USERS_CODE_GROUPS + _GROUP_ID + "/invite/{inviteCode}",
        "29946286-0f78-41fa-84fa-d3278d2a97c4", 2L, "e00672a2-77b2-484c-8a8e-78fa44d22d51")
    ).andExpect(status().isNoContent());
  }

  @Test
  public void shouldReturn204_whenRemoveInvite() throws Exception {
    mvc.perform(
      delete(_V1_USERS_CODE_GROUPS + _GROUP_ID + "/invite/{inviteCode}",
        "29946286-0f78-41fa-84fa-d3278d2a97c4", 2L, "e00672a2-77b2-484c-8a8e-78fa44d22d51")
    ).andExpect(status().isNoContent());
  }

  @Test
  public void shouldReturn403_whenNonAdminUpdatesGroup() throws Exception {
    mvc.perform(
      put(_V1_USERS_CODE_GROUPS + _GROUP_ID, VALID_USER_CODE, 2L)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(getResource("data/request/v1/groups/group-update-body.json"))
    ).andExpect(status().isForbidden())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/error-response/access-denied-error.json")));
  }

  @Test
  public void shouldReturn404_givenInvalidID() throws Exception {
    mvc.perform(
      get(_V1_USERS_CODE_GROUPS + _GROUP_ID, VALID_USER_CODE, 10000L)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isNotFound())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/error-response/group-not-found.json")));
  }

  @Test
  public void shouldReturn406_givenInvalidAcceptHeader() throws Exception {
    mvc.perform(get(_V1_USERS_CODE_GROUPS, VALID_USER_CODE).accept(MediaType.APPLICATION_XML)).andExpect(status().isNotAcceptable());
  }
}
