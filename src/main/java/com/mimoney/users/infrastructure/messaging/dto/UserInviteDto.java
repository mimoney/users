package com.mimoney.users.infrastructure.messaging.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UserInviteDto {

  private final String hostFullName;
  private final String guestFirstName;
  private final Long groupId;
  private final String groupName;
  private final String email;

}
