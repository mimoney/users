package com.mimoney.users.domain.service.impl;

import com.mimoney.users.domain.enumerated.ErrorCode;
import com.mimoney.users.domain.enumerated.TokenType;
import com.mimoney.users.domain.exception.BusinessRuleException;
import com.mimoney.users.domain.model.Token;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.repository.ITokenRepository;
import com.mimoney.users.domain.repository.IUserRepository;
import com.mimoney.users.domain.service.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.OffsetDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserAccessServiceTest {

  private static final String USER_CODE = "USER_CODE";
  private static final String TOKEN = "999999";
  private static final String USER_EMAIL = "user@email.com";
  private static final String USER_NAME = "user.name";
  private static final String USER_PASSWORD = "password";
  private static final String USER_NEW_PASSWORD = "new_password";
  private User user;
  private Token token;

  private UserAccessService fixture;

  @Mock
  private IUserService userService;
  @Mock
  private IUserRepository userRepository;
  @Mock
  private ITokenRepository tokenRepository;
  @Mock
  private PasswordEncoder encoder;
  @Mock
  private ApplicationEventPublisher publisher;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    fixture = new UserAccessService(publisher, userService, userRepository, tokenRepository, encoder);
    setUpUser();
    lenient().doNothing().when(publisher).publishEvent(any());
  }

  @Test
  public void shouldRegisterNewUser() {
    setUpToken(TokenType.REGISTRATION);
    when(userRepository.emailExists(USER_EMAIL)).thenReturn(false);
    when(userRepository.userNameExists(USER_NAME)).thenReturn(false);
    when(tokenRepository.save(any(Token.class))).thenReturn(token);

    fixture.registerNewUser(user);

    verify(encoder).encode(USER_PASSWORD);
    verify(userRepository).save(any(User.class));
    verify(tokenRepository).save(any(Token.class));
  }

  @Test
  public void shouldConfirmRegistration() {
    var token = new Token(user, TokenType.REGISTRATION);
    when(userService.getUserByCode(USER_CODE)).thenReturn(user);
    when(tokenRepository.getToken(TOKEN, TokenType.REGISTRATION, user))
      .thenReturn(Optional.of(token));

    fixture.confirmRegistration(USER_CODE, TOKEN);

    assertTrue(user.getActive());
    verify(tokenRepository).delete(token);
  }

  @Test
  public void shouldThrowBusinessRuleException_whenConfirmationTokenExpired() {
    var token = mock(Token.class);
    when(token.getExpirationDate()).thenReturn(OffsetDateTime.now().minusDays(5L));
    when(userService.getUserByCode(USER_CODE)).thenReturn(user);
    when(tokenRepository.getToken(TOKEN, TokenType.REGISTRATION, user)).thenReturn(Optional.of(token));

    BusinessRuleException ex = assertThrows(BusinessRuleException.class, () -> fixture.confirmRegistration(USER_CODE, TOKEN));

    assertEquals(ErrorCode.TOKEN_EXPIRED.getMessage(), ex.getMessage());
    verify(tokenRepository).delete(token);
  }

  @Test
  public void shouldRequestPasswordReset() {
    setUpToken(TokenType.PASSWORD);
    when(userRepository.findUserByEmail(USER_EMAIL)).thenReturn(Optional.of(user));
    when(tokenRepository.save(any(Token.class))).thenReturn(token);

    fixture.requestPasswordReset(USER_EMAIL);

    verify(tokenRepository).save(any(Token.class));
  }

  @Test
  public void shouldResetPassword() {
    var token = new Token(user, TokenType.PASSWORD);
    when(userService.getUserByCode(USER_CODE)).thenReturn(user);
    when(tokenRepository.getToken(TOKEN, TokenType.PASSWORD, user)).thenReturn(Optional.of(token));

    fixture.resetPassword(USER_CODE, TOKEN,USER_NEW_PASSWORD);

    verify(encoder).encode(USER_NEW_PASSWORD);
    verify(userRepository).save(user);
    verify(tokenRepository).delete(token);
  }

  @Test
  public void shouldThrowBusinessRuleException_whenPasswordTokenExpired() {
    var token = mock(Token.class);
    when(token.getExpirationDate()).thenReturn(OffsetDateTime.now().minusDays(5L));
    when(userService.getUserByCode(USER_CODE)).thenReturn(user);
    when(tokenRepository.getToken(TOKEN, TokenType.PASSWORD, user)).thenReturn(Optional.of(token));

    BusinessRuleException ex = assertThrows(BusinessRuleException.class, () -> fixture.resetPassword(USER_CODE, TOKEN, USER_NEW_PASSWORD));

    assertEquals(ErrorCode.TOKEN_EXPIRED.getMessage(), ex.getMessage());
    verify(tokenRepository).delete(token);
  }

  @Test
  public void shouldActivateUser() {
    when(userService.getUserByCode(USER_CODE)).thenReturn(user);

    fixture.activateUser(USER_CODE);

    assertTrue(user.getActive());
  }

  @Test
  public void shouldDeactivateUser() {
    when(userService.getUserByCode(USER_CODE)).thenReturn(user);

    fixture.deactivateUser(USER_CODE);

    assertFalse(user.getActive());
  }

  @Test
  public void shouldThrowBusinessRuleException_whenEmailNotFound() {
    when(userRepository.findUserByEmail(USER_EMAIL)).thenReturn(Optional.empty());

    assertThrows(BusinessRuleException.class, () -> fixture.requestPasswordReset(USER_EMAIL));
  }

  @Test
  public void shouldThrowBusinessRuleException_whenCheckingUserNameAndEmail() {
    setUpUser();
    when(userRepository.emailExists(USER_EMAIL)).thenReturn(true);
    when(userRepository.userNameExists(USER_NAME)).thenReturn(false);

    BusinessRuleException exception1 = assertThrows(BusinessRuleException.class, () -> fixture.registerNewUser(user));
    String emailMessage = "There already is an account using that email address.";
    assertTrue(exception1.getMessage().contains(emailMessage));

    when(userRepository.emailExists(USER_EMAIL)).thenReturn(false);
    when(userRepository.userNameExists(USER_NAME)).thenReturn(true);

    BusinessRuleException exception = assertThrows(BusinessRuleException.class, () -> fixture.registerNewUser(user));
    String usernameMessage = "There already is an account using that user name.";
    assertTrue(exception.getMessage().contains(usernameMessage));
  }

  private void setUpUser() {
    user = new User();
    user.setUserName(USER_NAME);
    user.setEmail(USER_EMAIL);
    user.setPassword(USER_PASSWORD);
  }

  private void setUpToken(TokenType type) {
    token = new Token(user, type);
  }
}
