package com.mimoney.users.api.v1.model.response;

import com.mimoney.users.domain.enumerated.ResourceType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Getter
@Setter
public class ResourceResponseV1 {

  @Schema(example = ParamExample.RESOURCE_ID)
  private Long id;

  @Schema(example = "Bank Loan")
  private String name;

  @Schema(example = "9999.9")
  private BigDecimal value;

  @Schema(implementation = ResourceType.class, example = "LOAN")
  private ResourceType type;

}
