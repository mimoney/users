package com.mimoney.users.api.v1.controller;

import com.mimoney.users.api.v1.model.mapper.UserDetailResponseV1Mapper;
import com.mimoney.users.api.v1.model.mapper.UserRegisterRequestV1Mapper;
import com.mimoney.users.api.v1.model.request.PasswordRequestV1;
import com.mimoney.users.api.v1.model.request.UserRegisterRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.api.v1.model.request.parameter.UserTokenParam;
import com.mimoney.users.core.config.ModelMapperConfig;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.service.IUserAccessService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class UserAccessControllerV1Test {

  private UserAccessControllerV1 fixture;
  private IUserAccessService userAccessService;

  @BeforeEach
  public void setUp() {
    ModelMapper mapper = new ModelMapperConfig().modelMapper();
    userAccessService = mock(IUserAccessService.class);
    fixture = new UserAccessControllerV1(
      userAccessService,
      new UserDetailResponseV1Mapper(mapper),
      new UserRegisterRequestV1Mapper(mapper)
    );
  }

  @Test
  public void registerNewUser() {
    when(userAccessService.registerNewUser(any(User.class))).thenReturn(mockUser());
    var response = fixture.registerNewUser(mockNewUser());

    verify(userAccessService).registerNewUser(any(User.class));
    assertNotNull(response);
  }

  @Test
  public void confirmUserRegistration() {
    fixture.confirmUserRegistration(mockUserTokenParam());
    verify(userAccessService).confirmRegistration(anyString(), anyString());
  }

  @Test
  public void requestPasswordReset() {
    fixture.requestPasswordReset("user.email@mimoney.com");
    verify(userAccessService).requestPasswordReset(anyString());
  }

  @Test
  public void resetPassword() {
    var password = new PasswordRequestV1();
    password.setPassword("PASSWORD");
    fixture.passwordReset(mockUserTokenParam(), password);
    verify(userAccessService).resetPassword(anyString(), anyString(), anyString());
  }

  @Test
  public void activateUser() {
    fixture.activateUser(mockUserParam());
    verify(userAccessService).activateUser(anyString());
  }

  @Test
  public void deactivateUser() {
    fixture.deactivateUser(mockUserParam());
    verify(userAccessService).deactivateUser(anyString());
  }

  private UserParam mockUserParam() {
    var param = new UserParam();
    param.setUserCode("UUID1");
    return param;
  }

  private UserTokenParam mockUserTokenParam() {
    var param = new UserTokenParam();
    param.setUserCode("UUID1");
    param.setToken("999999");
    return param;
  }

  private User mockUser() {
    return new User(1L, "UUID1", "userName1", "FirstName1", "LastName1",
      "user1@email.com", "PASSWORD1", OffsetDateTime.now(), OffsetDateTime.now(),
      false, new HashSet<>(), new ArrayList<>(), new ArrayList<>());
  }

  private UserRegisterRequestV1 mockNewUser() {
    var newUser = new UserRegisterRequestV1();
    newUser.setFirstName("FirstName1");
    newUser.setLastName("LastName1");
    newUser.setUserName("userName1");
    newUser.setEmail("user1@email.com");
    newUser.setPassword("PASSWORD1");
    newUser.setConfirmPassword("PASSWORD1");
    return newUser;
  }

}
