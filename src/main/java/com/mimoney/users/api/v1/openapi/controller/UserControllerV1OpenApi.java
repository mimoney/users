package com.mimoney.users.api.v1.openapi.controller;

import com.mimoney.users.api.v1.model.request.UserRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.api.v1.model.response.UserDetailResponseV1;
import com.mimoney.users.api.v1.model.response.UserResponseV1;
import com.mimoney.users.api.v1.openapi.OpenApiConstants;
import com.mimoney.users.api.v1.openapi.response.Ok200Response;
import com.mimoney.users.api.v1.openapi.response.error.GlobalErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;

import java.util.List;

@Tag(name = OpenApiConstants.Tag.TAG_USERS, description = "Users management operations.")
public interface UserControllerV1OpenApi {

  @Ok200Response
  @GlobalErrorResponse
  @Operation(summary = "List users", description = "Gets a list of all users.")
  List<UserResponseV1> getUsers();

  @Ok200Response
  @GlobalErrorResponse
  @Operation(summary = "Get user by code", description = "Gets a user's detailed data by its user code.")
  UserDetailResponseV1 getUser(@ParameterObject UserParam param);

  @Ok200Response
  @GlobalErrorResponse
  @Operation(summary = "Update user", description = "Updates an user's data by its user code.")
  UserDetailResponseV1 updateUser(@ParameterObject UserParam param, @RequestBody UserRequestV1 body);

}
