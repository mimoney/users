package com.mimoney.users.api.v1.controller;

import com.mimoney.users.api.v1.model.mapper.UserDetailResponseV1Mapper;
import com.mimoney.users.api.v1.model.mapper.UserRequestV1Mapper;
import com.mimoney.users.api.v1.model.mapper.UserResponseV1Mapper;
import com.mimoney.users.api.v1.model.request.UserRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.api.v1.model.response.UserDetailResponseV1;
import com.mimoney.users.api.v1.model.response.UserResponseV1;
import com.mimoney.users.api.v1.openapi.controller.UserControllerV1OpenApi;
import com.mimoney.users.domain.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.mimoney.users.core.commons.Constants.USERS;
import static com.mimoney.users.core.commons.Constants.V1;

@AllArgsConstructor
@RestController
@RequestMapping(path = V1 + USERS, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserControllerV1 implements UserControllerV1OpenApi {

  private final IUserService userService;
  private final UserResponseV1Mapper userResponseV1Mapper;
  private final UserDetailResponseV1Mapper userDetailResponseV1Mapper;
  private final UserRequestV1Mapper userRequestV1Mapper;

  @GetMapping
  public List<UserResponseV1> getUsers() {
    return userResponseV1Mapper.map(userService.getUsers());
  }

  @GetMapping("/{userCode}")
  public UserDetailResponseV1 getUser(@Valid UserParam param) {
    return userDetailResponseV1Mapper.map(userService.getUserByCode(param.getUserCode()));
  }

  @PutMapping("/{userCode}")
  public UserDetailResponseV1 updateUser(@Valid UserParam param, @Valid @RequestBody UserRequestV1 body) {
    var userUpdate = userService.getUserByCode(param.getUserCode());
    userRequestV1Mapper.copy(body, userUpdate);
    return userDetailResponseV1Mapper.map(userService.updateUser(userUpdate));
  }

}
