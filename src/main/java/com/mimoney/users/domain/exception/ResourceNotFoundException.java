package com.mimoney.users.domain.exception;

import com.mimoney.users.domain.enumerated.ErrorCode;

public class ResourceNotFoundException extends EntityNotFoundException {

  public ResourceNotFoundException(String resourceId, String userCode) {
    super(ErrorCode.RESOURCE_NOT_FOUND, resourceId, userCode);
  }

}
