package com.mimoney.users.domain.service;

import com.mimoney.users.domain.model.User;
import org.springframework.transaction.annotation.Transactional;

public interface IUserAccessService {

  @Transactional
  User registerNewUser(User user);

  @Transactional
  void confirmRegistration(String userCode, String activationToken);

  @Transactional
  void requestPasswordReset(String email);

  @Transactional
  void resetPassword(String userCode, String passwordToken, String newPassword);

  @Transactional
  void activateUser(String userCode);

  @Transactional
  void deactivateUser(String userCode);

}
