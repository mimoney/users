package com.mimoney.users.api.v1.controller;

import com.mimoney.users.UsersApiApplication;
import com.mimoney.users.domain.enumerated.TokenType;
import com.mimoney.users.domain.model.Token;
import com.mimoney.users.domain.repository.ITokenRepository;
import com.mimoney.users.domain.service.impl.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.mimoney.users.utils.TestUtils.getResource;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = UsersApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserAccessControllerV1IntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private ITokenRepository tokenRepository;
  @Autowired
  private UserService userService;

  private MockMvc mvc;

  @BeforeEach
  public void setUp() {
    mvc = setupMvc();
    runTestData();
  }

  @Test
  public void shouldReturn201_whenRegisterNewUser() throws Exception {
    mvc.perform(
      post(_V1_USERS + "/registration")
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(getResource("data/request/v1/users-access/register-new-user-body.json"))
    ).andExpect(status().isCreated())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.userName").value("user.register"))
      .andExpect(jsonPath("$.email").value("user.register@mimoney.com"))
      .andExpect(jsonPath("$.active").value(false));
  }

  @Test
  public void shouldReturn200_whenConfirmUserRegistration() throws Exception {
    var token = setupRegistrationToken();
    mvc.perform(
      get(_V1_USERS + _USER_CODE + "/confirmRegistration/{token}", VALID_USER_CODE, token)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isNoContent());
  }

  @Test
  public void shouldReturn204_whenRequestPasswordReset() throws Exception {
    mvc.perform(
      get(_V1_USERS + "/requestPassword?email=bacchetti@mimoney.com")
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isNoContent());
  }

  @Test
  public void shouldReturn204_whenResetPassword() throws Exception {
    var token = setupPasswordToken();
    mvc.perform(
      put(_V1_USERS + _USER_CODE + "/resetPassword/{token}", VALID_USER_CODE, token)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"password\":\"Pas$w0rd\",\"confirmPassword\":\"Pas$w0rd\"}")
    ).andExpect(status().isNoContent());
  }

  @Test
  public void shouldReturn204_whenActivateUser() throws Exception {
    mvc.perform(
      put(_V1_USERS + _USER_CODE + "/activation", VALID_USER_CODE)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isNoContent());
  }

  @Test
  public void shouldReturn204_whenDeactivateUser() throws Exception {
    mvc.perform(
      delete(_V1_USERS + _USER_CODE + "/deactivation", VALID_USER_CODE)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isNoContent());
  }

  private String setupRegistrationToken() {
    var user = userService.getUserByCode(VALID_USER_CODE);
    return tokenRepository.save(new Token(user, TokenType.REGISTRATION)).getTokenCode();
  }

  private String setupPasswordToken() {
    var user = userService.getUserByCode(VALID_USER_CODE);
    return tokenRepository.save(new Token(user, TokenType.PASSWORD)).getTokenCode();
  }

}
