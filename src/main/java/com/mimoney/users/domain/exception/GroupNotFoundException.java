package com.mimoney.users.domain.exception;

import com.mimoney.users.domain.enumerated.ErrorCode;

public class GroupNotFoundException extends EntityNotFoundException {

  public GroupNotFoundException(String groupId) {
    super(ErrorCode.GROUP_NOT_FOUND, groupId);
  }
}
