package com.mimoney.users.api.validation.validator;

import com.mimoney.users.api.validation.ValidUserCodeList;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.repository.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class UserCodeListValidator implements ConstraintValidator<ValidUserCodeList, Set<String>> {

  private final IUserRepository userRepository;
  private boolean nullable;

  @Override
  public void initialize(ValidUserCodeList constraintAnnotation) {
    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    this.nullable = constraintAnnotation.nullable();
  }

  @Override
  public boolean isValid(Set<String> value, ConstraintValidatorContext context) {
    if (Objects.isNull(value)) {
      return nullable;
    } else {
      List<User> users = this.userRepository.findUsersByCodes(value);
      Set<String> notFound = new HashSet<>();

      value.forEach(code -> {
        if (users.stream().noneMatch(user -> user.getUserCode().equals(code))) {
          notFound.add(code);
        }
      });

      if (notFound.isEmpty()) {
        return true;
      } else {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate("User code(s) not found: ".concat(String.join(", ", notFound)));
        return false;
      }
    }
  }
}
