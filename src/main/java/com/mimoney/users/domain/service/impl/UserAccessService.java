package com.mimoney.users.domain.service.impl;

import com.mimoney.users.core.commons.Constants;
import com.mimoney.users.domain.enumerated.ErrorCode;
import com.mimoney.users.domain.enumerated.TokenType;
import com.mimoney.users.domain.event.PasswordResetRequestedEvent;
import com.mimoney.users.domain.event.UserRegisteredEvent;
import com.mimoney.users.domain.exception.BusinessRuleException;
import com.mimoney.users.domain.exception.TokenNotFoundException;
import com.mimoney.users.domain.model.Token;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.repository.ITokenRepository;
import com.mimoney.users.domain.repository.IUserRepository;
import com.mimoney.users.domain.service.IUserAccessService;
import com.mimoney.users.domain.service.IUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.UUID;

@Slf4j
@Service
@AllArgsConstructor
public class UserAccessService implements IUserAccessService {

  private final ApplicationEventPublisher eventPublisher;
  private final IUserService userService;
  private final IUserRepository userRepository;
  private final ITokenRepository tokenRepository;
  private final PasswordEncoder encoder;

  @Override
  public User registerNewUser(User user) {
    checkUserNameAndEmail(user.getEmail(), user.getUserName());
    user.setUserCode(UUID.randomUUID().toString());
    user.setPassword(encoder.encode(user.getPassword()));
    user.setActive(Boolean.FALSE);
    log.info("[{}] [Registering new user] [Code: {}]", Constants.APP, user.getUserCode());
    userRepository.save(user);
    log.info("[{}] [Generating new registration token]", Constants.APP);
    var token = tokenRepository.save(new Token(user, TokenType.REGISTRATION));
    log.info("[{}] [Publishing user registry event]", Constants.APP);
    eventPublisher.publishEvent(new UserRegisteredEvent(token));
    return user;
  }

  @Override
  public void confirmRegistration(String userCode, String activationToken) {
    var user = userService.getUserByCode(userCode);
    var token = tokenRepository.getToken(activationToken, TokenType.REGISTRATION, user)
      .orElseThrow(() -> new TokenNotFoundException(activationToken));

    if (OffsetDateTime.now().isBefore(token.getExpirationDate())) {
      log.info("[{}] [Confirming user registration] [User: {}] [Token: {}]", Constants.APP, user.getId(), token.getTokenCode());
      user.setActive(Boolean.TRUE);
      tokenRepository.delete(token);
    } else {
      tokenRepository.delete(token);
      throw new BusinessRuleException(ErrorCode.TOKEN_EXPIRED);
    }
  }

  @Override
  public void requestPasswordReset(String email) {
    var user = userRepository.findUserByEmail(email)
      .orElseThrow(() -> new BusinessRuleException(ErrorCode.USER_EMAIL_NOT_EXISTS));
    log.info("[{}] [Generating new password reset token] [User: {}]", Constants.APP, user.getId());
    var token = tokenRepository.save(new Token(user, TokenType.PASSWORD));
    eventPublisher.publishEvent(new PasswordResetRequestedEvent(token));
  }

  @Override
  public void resetPassword(String userCode, String passwordToken, String newPassword) {
    var user = userService.getUserByCode(userCode);
    var token = tokenRepository.getToken(passwordToken, TokenType.PASSWORD, user)
      .orElseThrow(() -> new TokenNotFoundException(passwordToken));

    if (OffsetDateTime.now().isBefore(token.getExpirationDate())) {
      user.setPassword(encoder.encode(newPassword));
      log.info("[{}] [Resetting user password] [User: {}]", Constants.APP, user.getId());
      userRepository.save(user);
      tokenRepository.delete(token);
    } else {
      tokenRepository.delete(token);
      throw new BusinessRuleException(ErrorCode.TOKEN_EXPIRED);
    }
  }

  @Override
  public void activateUser(String userCode) {
    var user = userService.getUserByCode(userCode);
    log.info("[{}] [Activating] [User: {}]", Constants.APP, user.getId());
    user.setActive(Boolean.TRUE);
  }

  @Override
  public void deactivateUser(String userCode) {
    var user = userService.getUserByCode(userCode);
    log.info("[{}] [Deactivating user] [User: {}]", Constants.APP, user.getId());
    user.setActive(Boolean.FALSE);
  }

  private void checkUserNameAndEmail(String email, String userName) {
    if (userRepository.emailExists(email)) {
      throw new BusinessRuleException(ErrorCode.USER_EMAIL_EXISTS);
    }
    if (userRepository.userNameExists(userName)) {
      throw new BusinessRuleException(ErrorCode.USER_NAME_EXISTS);
    }
  }
}
