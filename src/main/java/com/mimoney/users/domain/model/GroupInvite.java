package com.mimoney.users.domain.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Objects;
import java.util.UUID;

@Data
@Entity
@Table(name = "user_group_invite")
@NoArgsConstructor
public class GroupInvite {

  @EmbeddedId
  private GroupInviteId id;

  @ManyToOne
  @JoinColumn(name = "host_id", nullable = false, insertable = false, updatable = false)
  private User host;

  @ManyToOne
  @JoinColumn(name = "guest_id", nullable = false, insertable = false, updatable = false)
  private User guest;

  @ManyToOne
  @JoinColumn(name = "user_group_id", nullable = false, insertable = false, updatable = false)
  private Group group;

  @Column(name = "invite_code", nullable = false)
  private String inviteCode;

  @CreationTimestamp
  @Column(name = "invitation_date", nullable = false)
  private OffsetDateTime invitationDate;

  public GroupInvite(User host, User guest, Group group) {
    this.id = new GroupInviteId(host.getId(), guest.getId(), group.getId());
    this.host = host;
    this.guest = guest;
    this.group = group;
    this.inviteCode = UUID.randomUUID().toString();
    this.invitationDate = OffsetDateTime.now();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof GroupInvite)) return false;
    GroupInvite that = (GroupInvite) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
