package com.mimoney.users.api.v1.model.request;

import com.mimoney.users.domain.enumerated.ResourceType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Getter
@Setter
public class ResourceRequestV1 {

  @Schema(example = "Bank Loan")
  @NotBlank
  private String name;

  @Schema(example = "9999.99")
  @NotNull
  @PositiveOrZero
  private BigDecimal value;

  @Schema(implementation = ResourceType.class, example = "LOAN")
  @NotNull
  private ResourceType type;

}
