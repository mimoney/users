package com.mimoney.users.api.v1.controller;

import com.mimoney.users.api.v1.model.mapper.UserDetailResponseV1Mapper;
import com.mimoney.users.api.v1.model.mapper.UserRequestV1Mapper;
import com.mimoney.users.api.v1.model.mapper.UserResponseV1Mapper;
import com.mimoney.users.api.v1.model.request.UserRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.core.config.ModelMapperConfig;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.service.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class UserControllerV1Test {

  private UserControllerV1 fixture;
  private IUserService userService;

  @BeforeEach
  public void setUp() {
    ModelMapper mapper = new ModelMapperConfig().modelMapper();
    userService = mock(IUserService.class);
    fixture = new UserControllerV1(
      userService,
      new UserResponseV1Mapper(mapper),
      new UserDetailResponseV1Mapper(mapper),
      new UserRequestV1Mapper(mapper)
    );

    when(userService.getUsers()).thenReturn(mockUsers());
    when(userService.getUserByCode(anyString())).thenReturn(mockUser("1"));
  }

  @Test
  public void getUsers() {
    var response = fixture.getUsers();

    verify(userService).getUsers();

    assertNotNull(response);
    assertEquals(3, response.size());
  }

  @Test
  public void getUser() {
    var param = new UserParam();
    param.setUserCode("UUID1");

    var response = fixture.getUser(param);

    verify(userService).getUserByCode(anyString());

    assertNotNull(response);
    assertEquals(param.getUserCode(), response.getCode());
  }

  @Test
  public void updateUser() {
    var param = mockUserParam();
    var update = mockUserRequest();
    var user = mockUser("1");

    when(userService.getUserByCode(param.getUserCode())).thenReturn(user);
    when(userService.updateUser(user)).thenReturn(user);

    var response = fixture.updateUser(param, update);

    verify(userService).getUserByCode(anyString());

    assertNotNull(response);
    assertEquals(param.getUserCode(), response.getCode());
    assertEquals(update.getFirstName(), response.getFirstName());
    assertEquals(update.getLastName(), response.getLastName());
  }

  private UserParam mockUserParam() {
    var param = new UserParam();
    param.setUserCode("UUID1");
    return param;
  }

  private UserRequestV1 mockUserRequest() {
    var update = new UserRequestV1();
    update.setFirstName("FirstName1Updated");
    update.setLastName("LastName1Updated");
    return update;
  }

  private User mockUser(String id) {
    return new User(Long.valueOf(id), "UUID".concat(id), "userName".concat(id), "FirstName".concat(id), "LastName".concat(id),
      String.format("user%s@email.com", id), "PASSWORD".concat(id), OffsetDateTime.now(), OffsetDateTime.now(),
      false, new HashSet<>(), new ArrayList<>(), new ArrayList<>());
  }

  private List<User> mockUsers() {
    return Arrays.asList(mockUser("1"), mockUser("2"), mockUser("3"));
  }
}
