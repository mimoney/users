package com.mimoney.users.api.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;
import com.mimoney.users.domain.enumerated.ErrorCode;
import com.mimoney.users.domain.exception.BusinessRuleException;
import com.mimoney.users.domain.exception.EntityInUseException;
import com.mimoney.users.domain.exception.UserNotFoundException;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.TypeMismatchException;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.*;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

import static com.fasterxml.jackson.databind.JsonMappingException.Reference;
import static com.mimoney.users.utils.TestUtils.assertJSON;
import static com.mimoney.users.utils.TestUtils.toJson;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ApiExceptionHandlerTest {

  private static final String REMOTE_ADDR = "127.0.0.1";
  private static final String REQUESTED_URL = "http://localhost:8180/users/endpoint";
  private static final String REQUESTED_QUERY = "query=test";
  private static final String INVALID_VALUE = "invalidValue";
  private static final String INVALID_PARAM = "invalidParam";
  private static final String INVALID_PROP = "invalidProperty";

  private ApiExceptionHandler fixture;

  @Mock
  private MessageSource messageSource;
  @Mock
  private HttpServletRequest request;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    fixture = new ApiExceptionHandler(messageSource, request);

    lenient().when(request.getRemoteAddr()).thenReturn(REMOTE_ADDR);
    lenient().when(request.getRequestURL()).thenReturn(new StringBuffer(REQUESTED_URL));
    lenient().when(request.getQueryString()).thenReturn(REQUESTED_QUERY);
  }

  @Test
  public void handlingUncaught() throws JSONException {
    var response = fixture.handleUncaught(new NullPointerException(), mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    assertJSON("data/error-response/internal-server-error.json", response.getBody());

    //DevNote: Just for code coverage purpose
    when(request.getQueryString()).thenReturn("");
    fixture.handleUncaught(new NullPointerException(), mock(WebRequest.class));
  }

  @Test
  public void handlingBusinessRuleException() throws JSONException {
    when(request.getQueryString()).thenReturn("");
    var response = fixture.handleBusinessRuleException(
      new BusinessRuleException(ErrorCode.BUSINESS_RULE_ERROR), mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertJSON("data/error-response/business-rule-error.json", response.getBody());
  }

  @Test
  public void handlingEntityNotFoundException() throws JSONException {
    var ex = new UserNotFoundException("INVALID_USER_CODE");
    assertEquals("User 'INVALID_USER_CODE' not found.", ex.getMessage());

    var response = fixture.handleEntityNotFoundException(ex, mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    assertJSON("data/error-response/user-not-found.json", response.getBody());
  }

  @Test
  public void handlingEntityInUseException() throws JSONException {
    var response = fixture.handleEntityInUseException(
      new EntityInUseException(ErrorCode.USER_ENTITY_IN_USE), mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
    assertJSON("data/error-response/user-in-use-error.json", response.getBody());
  }

  @Test
  public void handlingAccessDeniedException() throws JSONException {
    var response = fixture.handleAccessDenied(
      new AccessDeniedException(""), mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    assertJSON("data/error-response/access-denied-error.json", response.getBody());
  }

  @Test
  public void handlingNoHandlerFoundException() throws JSONException {
    var response = fixture.handleNoHandlerFoundException(
      new NoHandlerFoundException("GET", "/users/v1/endpoint", new HttpHeaders()),
      new HttpHeaders(), HttpStatus.BAD_REQUEST, mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertJSON("data/error-response/no-handler-found.json", response.getBody());
  }

  @Test
  public void handlingHttpMessageNotReadable() throws JSONException {
    var response = fixture.handleHttpMessageNotReadable(
      new HttpMessageNotReadableException("", mock(HttpInputMessage.class)),
      new HttpHeaders(), HttpStatus.BAD_REQUEST, mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertJSON("data/error-response/invalid-body-error.json", response.getBody());
  }

  @Test
  public void shouldDelegateToHandleInvalidFormatException() {
    var spyFixture = spy(fixture);
    var formatException = mock(InvalidFormatException.class);
    var notReadableException = new HttpMessageNotReadableException("", formatException, mock(HttpInputMessage.class));

    assertTrue(notReadableException.getRootCause() instanceof InvalidFormatException);
    doAnswer(invocation -> new ResponseEntity<>(HttpStatus.BAD_REQUEST))
      .when(spyFixture).handleInvalidFormatException(any(InvalidFormatException.class), any(HttpHeaders.class), any(WebRequest.class));

    spyFixture.handleHttpMessageNotReadable(notReadableException, new HttpHeaders(), HttpStatus.BAD_REQUEST, mock(WebRequest.class));
    verify(spyFixture).handleInvalidFormatException(any(InvalidFormatException.class), any(HttpHeaders.class), any(WebRequest.class));
  }

  @Test
  public void shouldDelegateToHandlePropertyBindingException() {
    var spyFixture = spy(fixture);
    var bindingException = mock(PropertyBindingException.class);
    var notReadableException = new HttpMessageNotReadableException("", bindingException, mock(HttpInputMessage.class));

    assertTrue(notReadableException.getRootCause() instanceof PropertyBindingException);
    doAnswer(invocation -> new ResponseEntity<>(HttpStatus.BAD_REQUEST))
      .when(spyFixture).handlePropertyBindingException(any(PropertyBindingException.class), any(HttpHeaders.class), any(WebRequest.class));

    spyFixture.handleHttpMessageNotReadable(notReadableException, new HttpHeaders(), HttpStatus.BAD_REQUEST, mock(WebRequest.class));
    verify(spyFixture).handlePropertyBindingException(any(PropertyBindingException.class), any(HttpHeaders.class), any(WebRequest.class));
  }

  @Test
  public void handlingInvalidFormatException() throws JSONException {
    var ex = mock(InvalidFormatException.class);
    doReturn(INVALID_VALUE).when(ex).getValue();
    doReturn(String.class).when(ex).getTargetType();

    var spyFixture = spy(fixture);
    doReturn(INVALID_PROP).when(spyFixture).getPropertyPath(any());

    var response = spyFixture.handleInvalidFormatException(ex, new HttpHeaders(), mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertJSON("data/error-response/invalid-format-error.json", response.getBody());
  }

  @Test
  public void shouldDelegateToSuperClassHandle() {
    var spyFixture = spy(fixture);
    var ex = new TypeMismatchException("String", BigDecimal.class);
    spyFixture.handleTypeMismatch(ex, new HttpHeaders(), HttpStatus.BAD_REQUEST, mock(WebRequest.class));
    verify(spyFixture, never()).handleMethodArgumentTypeMismatch(any(MethodArgumentTypeMismatchException.class), any(HttpHeaders.class), any(WebRequest.class));
  }

  @Test
  public void shouldDelegateToHandleMethodArgumentTypeMismatch() {
    var spyFixture = spy(fixture);
    var ex = mock(MethodArgumentTypeMismatchException.class);

    doAnswer(invocation -> new ResponseEntity<>(HttpStatus.BAD_REQUEST))
      .when(spyFixture).handleMethodArgumentTypeMismatch(any(MethodArgumentTypeMismatchException.class), any(HttpHeaders.class), any(WebRequest.class));

    spyFixture.handleTypeMismatch(ex, new HttpHeaders(), HttpStatus.BAD_REQUEST, mock(WebRequest.class));

    verify(spyFixture).handleMethodArgumentTypeMismatch(any(MethodArgumentTypeMismatchException.class), any(HttpHeaders.class), any(WebRequest.class));
  }

  @Test
  public void handlingMethodArgumentTypeMismatch() throws JSONException {
    var ex = mock(MethodArgumentTypeMismatchException.class);
    doReturn(INVALID_PARAM).when(ex).getName();
    doReturn(INVALID_VALUE).when(ex).getValue();
    doReturn(String.class).when(ex).getRequiredType();

    var response = fixture.handleMethodArgumentTypeMismatch(ex, new HttpHeaders(), mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertJSON("data/error-response/argument-type-error.json", response.getBody());
  }

  @Test
  public void handlingPropertyBindingException() throws JSONException {
    var spyFixture = spy(fixture);
    doReturn(INVALID_PROP).when(spyFixture).getPropertyPath(any());

    var response = spyFixture.handlePropertyBindingException(
      mock(PropertyBindingException.class), new HttpHeaders(), mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertJSON("data/error-response/property-bind-error.json", response.getBody());
  }

  @Test
  public void handleValidationException_whenBindException() throws JSONException {
    List<ObjectError> errors = new ArrayList<>();
    errors.add(new FieldError("userRegisterRequestV1", "email", "user.email", true, null, new Object[]{ErrorCode.INVALID_EMAIL}, null));

    BindingResult result = mock(BeanPropertyBindingResult.class);
    BindException ex = new BindException(result);
    when(result.getAllErrors()).thenReturn(errors);

    var response = fixture.handleBindException(ex, new HttpHeaders(), HttpStatus.BAD_REQUEST, mock(WebRequest.class));

    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertJSON("data/error-response/error-code-validation-error.json", response.getBody());
  }

  @Test
  public void handleValidationException_whenMethodArgumentNotValid() throws JSONException {
    when(request.getQueryString()).thenReturn("");
    var codes = new String[]{"userRegisterRequestV1.userName", "userName"};
    List<ObjectError> errors = new ArrayList<>();
    errors.add(new FieldError("userRegisterRequestV1", "userName", "", false, null,
      new Object[]{new DefaultMessageSourceResolvable(codes, "must not be blank")}, "must not be blank"));

    BindingResult result = mock(BeanPropertyBindingResult.class);
    MethodArgumentNotValidException ex = new MethodArgumentNotValidException(mock(MethodParameter.class), result);
    when(result.getAllErrors()).thenReturn(errors);
    when(messageSource.getMessage(any(MessageSourceResolvable.class), any(Locale.class))).thenReturn("must not be blank");

    var response = fixture.handleMethodArgumentNotValid(ex, new HttpHeaders(), HttpStatus.BAD_REQUEST, mock(WebRequest.class));

    System.out.println(toJson(response.getBody()));

    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertJSON("data/error-response/not-blank-validation-error.json", response.getBody());
  }

  @Test
  public void handlingExceptionInternal() throws JSONException {
    var ex = mock(Exception.class);
    var headers = new HttpHeaders();
    var status = HttpStatus.BAD_REQUEST;
    var request = mock(WebRequest.class);
    var errorString = "ERROR_STRING";
    ResponseEntity<Object> response;

    response = fixture.handleExceptionInternal(ex, null, headers, status, request);
    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertEquals(HttpStatus.BAD_REQUEST.getReasonPhrase(), ((ApiError) Objects.requireNonNull(response.getBody())).getTitle());

    response = fixture.handleExceptionInternal(ex, errorString, headers, status, request);
    assertNotNull(response);
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertEquals(errorString, ((ApiError) Objects.requireNonNull(response.getBody())).getTitle());
  }

  @Test
  public void shouldGetPropertyPath() {
    Object obj = new Object();
    var references = Arrays.asList(
      new Reference(obj, "field1"),
      new Reference(obj, "field2"),
      new Reference(obj, "field3")
    );
    var expectedPath = "field1.field2.field3";
    assertEquals(expectedPath, fixture.getPropertyPath(references));
  }

}
