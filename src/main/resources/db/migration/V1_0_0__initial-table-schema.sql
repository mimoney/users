create table if not exists user
(
    id            bigint auto_increment,
    user_code     varchar(36)  not null,
    user_name     varchar(80)  not null,
    first_name    varchar(30)  not null,
    last_name     varchar(70)  not null,
    email         varchar(100) not null,
    password      varchar(60)  not null,
    creation_date datetime     not null,
    update_date   datetime,
    active        boolean default false,

    constraint user_pk
        primary key (id)
) engine = InnoDB
  default charset = utf8;

create unique index user_email_uindex
    on `user` (email);

create unique index user_user_name_uindex
    on `user` (user_name);

create table if not exists token
(
    token           char(8)     not null,
    type            varchar(15) not null,
    user_id         bigint      not null,
    expiration_date datetime    not null,

    constraint token_pk
        primary key (token),
    constraint token_user_fk
        foreign key (user_id) references user (id)
) engine = InnoDB
  default charset = utf8;

create table if not exists user_group
(
    id          bigint auto_increment,
    name        varchar(80) not null,
    description varchar(255),

    constraint user_group_pk
        primary key (id)
) engine = InnoDB
  default charset = utf8;

create table if not exists resource
(
    user_id bigint         not null,
    id      bigint         not null,
    name    varchar(80)    not null,
    value   DECIMAL(10, 2) not null,
    type    char(15)       not null,

    constraint resource_pk
        primary key (user_id, id),
    constraint resource_user_fk
        foreign key (user_id) references user (id)
) engine = InnoDB
  default charset = utf8;

create table if not exists users_groups
(
    user_id       bigint not null,
    user_group_id bigint not null,
    admin         boolean default false,

    constraint users_groups_pk
        primary key (user_id, user_group_id),
    constraint user_group_user_fk
        foreign key (user_id) references user (id),
    constraint user_group_group_fk
        foreign key (user_group_id) references user_group (id)
) engine = InnoDB
  default charset = utf8;

create table if not exists user_group_invite
(
    host_id         bigint      not null,
    guest_id        bigint      not null,
    user_group_id   bigint      not null,
    invite_code     varchar(36) not null,
    invitation_date datetime    not null,

    constraint user_group_invite_pk
        primary key (host_id, guest_id, user_group_id),
    constraint group_invite_host_fk
        foreign key (host_id) references user (id),
    constraint group_invite_guest_fk
        foreign key (guest_id) references user (id),
    constraint group_invite_user_group_fk
        foreign key (user_group_id) references user_group (id)
) engine = InnoDB
  default charset = utf8;
