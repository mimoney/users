package com.mimoney.users.infrastructure.messaging.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PasswordResetDto {

  private final String firstName;
  private final String lastName;
  private final String userCode;
  private final String email;
  private final String token;

}
