package com.mimoney.users.domain.listener;

import com.mimoney.users.domain.event.UserInvitedEvent;
import com.mimoney.users.domain.service.INotificationService;
import com.mimoney.users.infrastructure.messaging.dto.UserInviteDto;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@AllArgsConstructor
public class UserInvitedEventListener implements ApplicationListener<UserInvitedEvent> {

  private final INotificationService notificationService;

  @Override
  @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
  public void onApplicationEvent(UserInvitedEvent event) {
    var invite = event.getGroupInvite();
    notificationService.sendGroupInviteNotification(UserInviteDto.builder()
      .hostFullName(invite.getHost().getFirstName() + invite.getHost().getLastName())
      .guestFirstName(invite.getGuest().getFirstName())
      .groupId(invite.getGroup().getId())
      .groupName(invite.getGroup().getName())
      .email(invite.getGuest().getEmail())
      .build());
  }
}
