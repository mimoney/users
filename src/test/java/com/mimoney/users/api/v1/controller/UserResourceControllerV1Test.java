package com.mimoney.users.api.v1.controller;

import com.mimoney.users.api.v1.model.mapper.ResourceRequestV1Mapper;
import com.mimoney.users.api.v1.model.mapper.ResourceResponseV1Mapper;
import com.mimoney.users.api.v1.model.request.ResourceRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.ResourceParam;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.core.config.ModelMapperConfig;
import com.mimoney.users.domain.enumerated.ResourceType;
import com.mimoney.users.domain.model.Resource;
import com.mimoney.users.domain.model.ResourceId;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.service.IResourceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class UserResourceControllerV1Test {

  private UserResourceControllerV1 fixture;
  private IResourceService resourceService;

  @BeforeEach
  public void setUp() {
    ModelMapper mapper = new ModelMapperConfig().modelMapper();
    resourceService = mock(IResourceService.class);
    fixture = new UserResourceControllerV1(
      resourceService,
      new ResourceResponseV1Mapper(mapper),
      new ResourceRequestV1Mapper(mapper)
    );
  }

  @Test
  public void getUserResources() {
    var param = mockUserParam();
    when(resourceService.getUserResources(anyString())).thenReturn(mockResources());

    var response = fixture.getUserResources(param);

    verify(resourceService).getUserResources(anyString());
    assertNotNull(response);
    assertEquals(3, response.size());
  }

  @Test
  public void addUserResource() {
    var param = mockUserParam();
    var create = mockResourceCreate();
    var resource = mockResource("1");
    when(resourceService.addUserResource(anyString(), any(Resource.class))).thenReturn(resource);

    var response = fixture.addUserResource(param, create);

    verify(resourceService).addUserResource(anyString(), any(Resource.class));
    assertNotNull(response);
    assertEquals(Long.valueOf("1"), response.getId());
    assertEquals(create.getName(), response.getName());
  }

  @Test
  public void updateResource() {
    var param = mockResourceParam();
    var update = mockResourceUpdate();
    var resource = mockResource("1");

    when(resourceService.getUserResourceById(param.getUserCode(), param.getResourceId())).thenReturn(resource);
    when(resourceService.updateUserResource(resource)).thenReturn(resource);

    var response = fixture.updateUserResource(param, update);

    verify(resourceService).getUserResourceById(anyString(), anyLong());
    verify(resourceService).updateUserResource(any(Resource.class));
    assertNotNull(response);
    assertEquals(param.getResourceId(), response.getId());
    assertEquals(update.getName(), response.getName());
  }

  @Test
  public void removeUserResource() {
    var param = mockResourceParam();
    fixture.removeUserResource(param);

    verify(resourceService).removeUserResource(anyString(), anyLong());
  }


  private UserParam mockUserParam() {
    var param = new UserParam();
    param.setUserCode("UUID1");
    return param;
  }

  private ResourceParam mockResourceParam() {
    var param = new ResourceParam();
    param.setUserCode("UUID1");
    param.setResourceId(1L);
    return param;
  }

  private ResourceRequestV1 mockResourceCreate() {
    var create = new ResourceRequestV1();
    create.setName("ResourceName1");
    create.setValue(new BigDecimal(0));
    create.setType(ResourceType.OTHERS);
    return create;
  }

  private ResourceRequestV1 mockResourceUpdate() {
    var create = new ResourceRequestV1();
    create.setName("ResourceName1Updated");
    create.setValue(new BigDecimal(0));
    create.setType(ResourceType.OTHERS);
    return create;
  }

  private Resource mockResource(String id) {
    var resourceId = new ResourceId(1L, Long.valueOf(id));
    return new Resource(resourceId, "ResourceName".concat(id), new BigDecimal(0), ResourceType.OTHERS, new User());
  }

  private List<Resource> mockResources() {
    return Arrays.asList(mockResource("1"), mockResource("2"), mockResource("3"));
  }
}
