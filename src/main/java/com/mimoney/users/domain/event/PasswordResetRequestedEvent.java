package com.mimoney.users.domain.event;

import com.mimoney.users.domain.model.Token;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class PasswordResetRequestedEvent extends ApplicationEvent {

  private final Token token;

  public PasswordResetRequestedEvent(Token token) {
    super(token);
    this.token = token;
  }
}
