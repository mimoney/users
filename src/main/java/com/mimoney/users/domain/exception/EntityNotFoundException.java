package com.mimoney.users.domain.exception;

import com.mimoney.users.domain.enumerated.ErrorCode;
import lombok.Getter;

@Getter
public abstract class EntityNotFoundException extends RuntimeException {

  protected final ErrorCode errorCode;

  public EntityNotFoundException(ErrorCode errorCode, Object... args) {
    super(String.format(errorCode.getMessage(), args));
    this.errorCode = errorCode;
  }
}
