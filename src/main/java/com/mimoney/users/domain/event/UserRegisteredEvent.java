package com.mimoney.users.domain.event;

import com.mimoney.users.domain.model.Token;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class UserRegisteredEvent extends ApplicationEvent {

  private final Token token;

  public UserRegisteredEvent(Token token) {
    super(token);
    this.token = token;
  }
}
