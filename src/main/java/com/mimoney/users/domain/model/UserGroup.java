package com.mimoney.users.domain.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.Objects;

@Data
@NoArgsConstructor
@Entity
@Table(name = "users_groups")
public class UserGroup {

  @EmbeddedId
  private UserGroupId id;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id", nullable = false, insertable = false, updatable = false)
  private User user;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_group_id", nullable = false, insertable = false, updatable = false)
  private Group group;

  @Column(name = "admin")
  private boolean admin;

  public UserGroup(@NonNull User user, @NonNull Group group, boolean admin) {
    this.id = new UserGroupId(user.getId(), group.getId());
    this.user = user;
    this.group = group;
    this.admin = admin;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof UserGroup)) return false;
    UserGroup userGroup = (UserGroup) o;
    return id.equals(userGroup.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
