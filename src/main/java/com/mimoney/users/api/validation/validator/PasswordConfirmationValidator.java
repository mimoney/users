package com.mimoney.users.api.validation.validator;

import com.mimoney.users.api.validation.ValidPasswordConfirmation;
import com.mimoney.users.api.validation.helper.PasswordConfirmationHelper;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

@Component
public class PasswordConfirmationValidator implements ConstraintValidator<ValidPasswordConfirmation, PasswordConfirmationHelper> {

  @Override
  public boolean isValid(PasswordConfirmationHelper value, ConstraintValidatorContext context) {
    return Objects.equals(value.getPassword(), value.getConfirmPassword());
  }
}
