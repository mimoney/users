package com.mimoney.users.api.validation;

import com.mimoney.users.api.validation.validator.PasswordConfirmationValidator;
import com.mimoney.users.domain.enumerated.ErrorCode;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = PasswordConfirmationValidator.class)
public @interface ValidPasswordConfirmation {

  String message() default "";

  ErrorCode error() default ErrorCode.INVALID_PASSWORD_CONFIRMATION;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
