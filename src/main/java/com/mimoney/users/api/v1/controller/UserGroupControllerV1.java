package com.mimoney.users.api.v1.controller;

import com.mimoney.users.api.v1.model.mapper.GroupDetailResponseV1Mapper;
import com.mimoney.users.api.v1.model.mapper.GroupRequestV1Mapper;
import com.mimoney.users.api.v1.model.mapper.GroupResponseV1Mapper;
import com.mimoney.users.api.v1.model.request.GroupRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.GroupInviteParam;
import com.mimoney.users.api.v1.model.request.parameter.GroupParam;
import com.mimoney.users.api.v1.model.request.parameter.InviteParam;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.api.v1.model.response.GroupDetailResponseV1;
import com.mimoney.users.api.v1.model.response.GroupResponseV1;
import com.mimoney.users.api.v1.openapi.controller.UserGroupControllerV1OpenApi;
import com.mimoney.users.domain.service.IGroupService;
import com.mimoney.users.domain.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.mimoney.users.core.commons.Constants.GROUPS;
import static com.mimoney.users.core.commons.Constants.V1;

@AllArgsConstructor
@RestController
@RequestMapping(path = V1 + GROUPS, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserGroupControllerV1 implements UserGroupControllerV1OpenApi {

  private final IGroupService groupService;
  private final IUserService userService;
  private final GroupResponseV1Mapper groupResponseV1Mapper;
  private final GroupRequestV1Mapper groupRequestV1Mapper;
  private final GroupDetailResponseV1Mapper groupDetailResponseV1Mapper;

  @GetMapping
  public List<GroupResponseV1> getUserGroups(@Valid UserParam param) {
    return groupResponseV1Mapper.map(groupService.getUserGroups(param.getUserCode()));
  }

  @GetMapping("/{groupId}")
  public GroupDetailResponseV1 getGroup(@Valid GroupParam param) {
    userService.getUserByCode(param.getUserCode());
    return groupDetailResponseV1Mapper.map(groupService.getGroupById(param.getGroupId()));
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public GroupDetailResponseV1 createGroup(@Valid UserParam param, @RequestBody GroupRequestV1 body) {
    var user = userService.getUserByCode(param.getUserCode());
    var group = groupRequestV1Mapper.map(body);
    return groupDetailResponseV1Mapper.map(groupService.createGroup(user, group));
  }

  @PutMapping("/{groupId}")
  public GroupDetailResponseV1 updateGroup(@Valid GroupParam param, @RequestBody GroupRequestV1 body) {
    var user = userService.getUserByCode(param.getUserCode());
    var group = groupService.getGroupById(param.getGroupId());
    groupRequestV1Mapper.copy(body, group);
    return groupDetailResponseV1Mapper.map(groupService.updateGroup(user, group));
  }

  @DeleteMapping("/{groupId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void leaveGroup(@Valid GroupParam param) {
    var user = userService.getUserByCode(param.getUserCode());
    var group = groupService.getGroupById(param.getGroupId());
    groupService.leaveGroup(user, group);
  }

  @GetMapping("/{groupId}/invite")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void inviteToGroup(@Valid GroupInviteParam param) {
    var user = userService.getUserByCode(param.getUserCode());
    var group = groupService.getGroupById(param.getGroupId());
    groupService.inviteToGroup(user, group, param.getUserCodes());
  }

  @PutMapping("/{groupId}/invite/{inviteCode}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void acceptInvite(@Valid InviteParam param) {
    var user = userService.getUserByCode(param.getUserCode());
    groupService.acceptInvite(user, param.getInviteCode());
  }

  @DeleteMapping("/{groupId}/invite/{inviteCode}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void removeInvite(@Valid InviteParam param) {
    var user = userService.getUserByCode(param.getUserCode());
    groupService.removeInvite(user, param.getInviteCode());
  }
}
