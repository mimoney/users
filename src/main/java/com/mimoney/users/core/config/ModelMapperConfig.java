package com.mimoney.users.core.config;

import com.mimoney.users.api.v1.model.request.UserRequestV1;
import com.mimoney.users.api.v1.model.response.GroupShortResponseV1;
import com.mimoney.users.api.v1.model.response.ResourceResponseV1;
import com.mimoney.users.api.v1.model.response.UserShortResponseV1;
import com.mimoney.users.domain.model.Resource;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.model.UserGroup;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

  @Bean
  public ModelMapper modelMapper() {
    var modelMapper = new ModelMapper();
    mappingConfigV1(modelMapper);
    return modelMapper;
  }

  private void mappingConfigV1(ModelMapper modelMapper) {
    modelMapper.addMappings(new UserGroupToGroupShortResponseV1());
    modelMapper.addMappings(new UserGroupToUserShortResponseV1());
    modelMapper.addMappings(new ResourceToResourceResponseV1());
    modelMapper.addMappings(new UserRequestV1ToUser());
  }

  private static class UserGroupToGroupShortResponseV1 extends PropertyMap<UserGroup, GroupShortResponseV1> {
    @Override
    protected void configure() {
      map().setId(source.getId().getGroupId());
      map().setName(source.getGroup().getName());
      map().setDescription(source.getGroup().getDescription());
    }
  }

  private static class UserGroupToUserShortResponseV1 extends PropertyMap<UserGroup, UserShortResponseV1> {
    @Override
    protected void configure() {
      map().setCode(source.getUser().getUserCode());
      map().setUserName(source.getUser().getUserName());
      map().setFirstName(source.getUser().getFirstName());
      map().setLastName(source.getUser().getLastName());
      map().setEmail(source.getUser().getEmail());
      map().setAdmin(source.isAdmin());
    }
  }

  private static class ResourceToResourceResponseV1 extends PropertyMap<Resource, ResourceResponseV1> {
    @Override
    protected void configure() {
      map().setId(source.getId().getId());
      map().setName(source.getName());
      map().setValue(source.getValue());
      map().setType(source.getType());
    }
  }

  private static class UserRequestV1ToUser extends PropertyMap<UserRequestV1, User> {
    @Override
    protected void configure() {
      skip(destination.getUserName());
    }
  }

}
