package com.mimoney.users.domain.repository;

import com.mimoney.users.domain.model.Resource;
import com.mimoney.users.domain.model.ResourceId;
import com.mimoney.users.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IResourceRepository extends JpaRepository<Resource, ResourceId> {

  @Query("select case when coalesce(max(r.id.id), 0) > 0 then (max(r.id.id)+1) else 1 end from Resource r WHERE r.user = :user")
  Long getNextId(User user);

  List<Resource> findByUser(User user);

}
