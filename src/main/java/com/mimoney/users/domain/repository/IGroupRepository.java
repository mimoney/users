package com.mimoney.users.domain.repository;

import com.mimoney.users.domain.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IGroupRepository extends JpaRepository<Group, Long> {

  @Query("from Group g join UserGroup ug on g.id = ug.group.id where ug.user.id = :userId")
  List<Group> findAllByUser(Long userId);

}
