package com.mimoney.users.api.v1.openapi.response.error;

import com.mimoney.users.api.exception.ApiError;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

@Target({METHOD, TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@ApiResponse(
  responseCode = "400",
  description = "Bad Request - Something went wrong with the request, might be a validation error or a business rule violation.",
  content =
  @Content(
    mediaType = MediaType.APPLICATION_JSON_VALUE,
    schema = @Schema(implementation = ApiError.class),
    examples = {
      @ExampleObject(
        "{\n" +
          "  \"errorCode\" : 9994,\n" +
          "  \"title\" : \"Invalid Data\",\n" +
          "  \"message\" : \"Something is wrong. Check the request and try again.\",\n" +
          "  \"objects\" : [ {\n" +
          "    \"errorCode\" : 9999,\n" +
          "    \"name\" : \"parameterName\",\n" +
          "    \"message\" : \"Validation error message.\"\n" +
          "  } ]\n" +
          "}\n")
    })
)
public @interface BadRequest400Response {
}
