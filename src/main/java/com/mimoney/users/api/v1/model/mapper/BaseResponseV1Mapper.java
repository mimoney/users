package com.mimoney.users.api.v1.model.mapper;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BaseResponseV1Mapper<S, T> {

  private final ModelMapper mapper;

  public T map(S source) {
    return mapper.map(source, getTarget());
  }

  public List<T> map(List<S> source) {
    return source.stream().map(this::map).collect(Collectors.toList());
  }

  @SuppressWarnings("unchecked")
  private Class<T> getTarget() {
    ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
    return (Class<T>) parameterizedType.getActualTypeArguments()[1];
  }
}

