package com.mimoney.users.api.v1.openapi.response;

import com.mimoney.users.api.v1.openapi.response.error.AccessDenied403Response;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

@Target({METHOD, TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@AccessDenied403Response
@ApiResponse(responseCode = "200", description = "OK - Successfully completed.")
public @interface Ok200Response {
}
