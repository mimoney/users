package com.mimoney.users.api.v1.model.mapper;

import com.mimoney.users.api.v1.model.request.ResourceRequestV1;
import com.mimoney.users.domain.model.Resource;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ResourceRequestV1Mapper extends BaseRequestV1Mapper<ResourceRequestV1, Resource> {

  public ResourceRequestV1Mapper(final ModelMapper mapper) {
    super(mapper);
  }
}
