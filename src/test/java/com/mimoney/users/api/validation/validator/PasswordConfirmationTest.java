package com.mimoney.users.api.validation.validator;

import com.mimoney.users.api.validation.helper.PasswordConfirmationHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PasswordConfirmationTest {

  @InjectMocks
  private PasswordConfirmationValidator fixture;

  @Mock
  private ConstraintValidatorContext context;

  @Mock
  private PasswordConfirmationHelper value;

  @Test
  public void isValid() {
    when(value.getPassword()).thenReturn("Pa$$w0rd");
    when(value.getConfirmPassword()).thenReturn("Pa$$w0rd");

    assertTrue(fixture.isValid(value, context));
  }

  @Test
  public void isNotValid() {
    when(value.getPassword()).thenReturn("Pa$$w0rd");
    when(value.getConfirmPassword()).thenReturn(null);
    assertFalse(fixture.isValid(value, context));

    when(value.getPassword()).thenReturn(null);
    when(value.getConfirmPassword()).thenReturn("Pa$$w0rd");
    assertFalse(fixture.isValid(value, context));

    when(value.getPassword()).thenReturn("Pa$$w0rd");
    when(value.getConfirmPassword()).thenReturn("password");
    assertFalse(fixture.isValid(value, context));
  }

}
