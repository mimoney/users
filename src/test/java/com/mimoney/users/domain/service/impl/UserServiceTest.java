package com.mimoney.users.domain.service.impl;

import com.mimoney.users.domain.exception.UserNotFoundException;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.repository.IUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

  private static final String VALID_CODE = "VALID_CODE";

  private UserService fixture;

  @Mock
  private IUserRepository userRepository;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    fixture = new UserService(userRepository);
  }

  @Test
  public void shouldGetUsers() {
    var mockList = Arrays.asList(new User(), new User());
    when(userRepository.findAll()).thenReturn(mockList);

    var result = fixture.getUsers();

    verify(userRepository).findAll();
    assertEquals(mockList.size(), result.size());
  }

  @Test
  public void shouldGetUsersByCodes() {
    var codes = Set.of("CODE");
    var mockList = Collections.singletonList(new User());
    when(userRepository.findUsersByCodes(codes)).thenReturn(mockList);

    var result = fixture.getUsersByCodes(codes);

    verify(userRepository).findUsersByCodes(anySet());
    assertEquals(1, mockList.size());
  }

  @Test
  public void shouldGetUserByCode() {
    when(userRepository.findUserByUserCode(VALID_CODE)).thenReturn(Optional.of(new User()));

    var result = fixture.getUserByCode(VALID_CODE);

    verify(userRepository).findUserByUserCode(VALID_CODE);
    assertNotNull(result);
  }

  @Test
  public void shouldUpdateUser() {
    when(userRepository.save(any(User.class))).thenReturn(new User());

    var result = fixture.updateUser(mock(User.class));

    verify(userRepository).save(any(User.class));
    assertNotNull(result);
  }

  @Test
  public void shouldThrowUserNotFoundException() {
    when(userRepository.findUserByUserCode(anyString())).thenReturn(Optional.empty());

    assertThrows(UserNotFoundException.class, () -> fixture.getUserByCode(anyString()));
  }

}
