package com.mimoney.users.domain.repository;

import com.mimoney.users.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface IUserRepository extends JpaRepository<User, Long> {

  Optional<User> findUserByEmail(String email);

  Optional<User> findUserByUserCode(String userCode);

  @Query("from User u where u.userCode in :userCodes")
  List<User> findUsersByCodes(Set<String> userCodes);

  @Query("select case when count (u.id) > 0 then true else false end from User u WHERE u.email = :email")
  boolean emailExists(String email);

  @Query("select case when count (u.id) > 0 then true else false end from User u WHERE u.userName = :userName")
  boolean userNameExists(String userName);

}
