package com.mimoney.users.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class TestUtils {

  public static String getResource(String resourcePath) {
    try {
      Path path = ResourceUtils.getFile("src/test/resources/" + resourcePath).toPath();
      InputStream stream = Files.newInputStream(path);
      return StreamUtils.copyToString(stream, StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static Object toObject(String value) {
    try {
      return new ObjectMapper().readValue(value, Object.class);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  public static String toJson(Object value) {
    try {
      return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(value);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  public static void assertJSON(String expectedResponsePath, Object actualResponse) throws JSONException {
    Assertions.assertNotNull(actualResponse);
    String actualResponsePayload = toJson(actualResponse);
    Assertions.assertNotNull(actualResponsePayload);

    String expectedResponsePayload = getResource(expectedResponsePath);

    JSONAssert.assertEquals(expectedResponsePayload, actualResponsePayload,
      new CustomComparator(JSONCompareMode.LENIENT,
        new Customization("sessionId", (o1, o2) -> true)));
  }

}
