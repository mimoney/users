package com.mimoney.users.domain.service;

import com.mimoney.users.domain.model.Resource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IResourceService {

  List<Resource> getUserResources(String userCode);

  Resource getUserResourceById(String userCode, Long resourceId);

  @Transactional
  Resource addUserResource(String userCode, Resource resource);

  @Transactional
  Resource updateUserResource(Resource resource);

  @Transactional
  void removeUserResource(String userCode, Long resourceId);

}
