package com.mimoney.users.domain.service.impl;

import com.mimoney.users.core.commons.Constants;
import com.mimoney.users.domain.exception.UserNotFoundException;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.repository.IUserRepository;
import com.mimoney.users.domain.service.IUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor
public class UserService implements IUserService {

  private final IUserRepository userRepository;

  @Override
  public List<User> getUsers() {
    return userRepository.findAll();
  }

  @Override
  public List<User> getUsersByCodes(Set<String> userCodes) {
    return userRepository.findUsersByCodes(userCodes);
  }

  @Override
  public User getUserByCode(String userCode) {
    return userRepository.findUserByUserCode(userCode).orElseThrow(() -> new UserNotFoundException(userCode));
  }

  @Override
  public User updateUser(User user) {
    log.info("[{}] [Updating user] [User: {}]", Constants.APP, user.getId());
    return userRepository.save(user);
  }
}

