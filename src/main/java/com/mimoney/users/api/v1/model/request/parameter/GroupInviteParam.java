package com.mimoney.users.api.v1.model.request.parameter;

import com.mimoney.users.api.validation.ValidUserCodeList;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamDescription;
import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Hidden
@Getter
@Setter
public class GroupInviteParam extends GroupParam {

  @ValidUserCodeList
  @Parameter(in = ParameterIn.QUERY, description = ParamDescription.USERS_CODES, example = ParamExample.USERS_CODES)
  private Set<String> userCodes;

}
