package com.mimoney.users.domain.listener;

import com.mimoney.users.domain.event.UserInvitedEvent;
import com.mimoney.users.domain.model.GroupInvite;
import com.mimoney.users.domain.service.INotificationService;
import com.mimoney.users.infrastructure.messaging.dto.UserInviteDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserInvitedEventListenerTest {

  @InjectMocks
  private UserInvitedEventListener fixture;
  @Mock
  private INotificationService notificationService;

  @Test
  void handleEvent() {
    fixture.onApplicationEvent(new UserInvitedEvent(Mockito.mock(GroupInvite.class, Mockito.RETURNS_DEEP_STUBS)));
    Mockito.verify(notificationService).sendGroupInviteNotification(Mockito.any(UserInviteDto.class));
  }
}
