package com.mimoney.users.api.v1.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Getter
@Setter
public class GroupShortResponseV1 {

  @Schema(example = ParamExample.GROUP_ID)
  private Long id;

  @Schema(example = "Home Expenses")
  private String name;

  @Schema(example = "Group to share home expenses with family")
  private String description;

  @Schema(example = "true")
  private boolean admin;

}
