package com.mimoney.users.api.v1.model.request.parameter;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamDescription;
import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Hidden
@Getter
@Setter
public class UserParam {

  @Parameter(in = ParameterIn.PATH, description = ParamDescription.USER_CODE, example = ParamExample.USER_CODE)
  @NotBlank
  private String userCode;

}
