package com.mimoney.users.api.v1.controller;

import com.mimoney.users.api.v1.model.mapper.GroupDetailResponseV1Mapper;
import com.mimoney.users.api.v1.model.mapper.GroupRequestV1Mapper;
import com.mimoney.users.api.v1.model.mapper.GroupResponseV1Mapper;
import com.mimoney.users.api.v1.model.request.GroupRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.GroupInviteParam;
import com.mimoney.users.api.v1.model.request.parameter.GroupParam;
import com.mimoney.users.api.v1.model.request.parameter.InviteParam;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.core.config.ModelMapperConfig;
import com.mimoney.users.domain.model.Group;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.service.IGroupService;
import com.mimoney.users.domain.service.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.time.OffsetDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class UserGroupControllerV1Test {

  private UserGroupControllerV1 fixture;
  private IGroupService groupService;
  private IUserService userService;


  @BeforeEach
  public void setUp() {
    ModelMapper mapper = new ModelMapperConfig().modelMapper();
    groupService = mock(IGroupService.class);
    userService = mock(IUserService.class);
    fixture = new UserGroupControllerV1(
      groupService,
      userService,
      new GroupResponseV1Mapper(mapper),
      new GroupRequestV1Mapper(mapper),
      new GroupDetailResponseV1Mapper(mapper)
    );

    when(userService.getUserByCode(anyString())).thenReturn(mockUser());
  }

  @Test
  public void getUserGroups() {
    var param = mockUserParam();
    when(groupService.getUserGroups(anyString())).thenReturn(mockGroups());

    var response = fixture.getUserGroups(param);

    verify(groupService).getUserGroups(anyString());
    assertNotNull(response);
    assertEquals(3, response.size());
  }

  @Test
  public void getGroup() {
    var param = mockGroupParam();
    when(groupService.getGroupById(param.getGroupId())).thenReturn(mockGroup("1"));

    var response = fixture.getGroup(param);

    verify(userService).getUserByCode(anyString());
    verify(groupService).getGroupById(anyLong());
    assertNotNull(response);
    assertEquals(param.getGroupId(), response.getId());
  }

  @Test
  public void createGroup() {
    var param = mockUserParam();
    var create = mockGroupCreate();

    when(groupService.createGroup(any(User.class), any(Group.class))).thenReturn(mockGroup("1"));
    var response = fixture.createGroup(param, create);

    verify(userService).getUserByCode(anyString());
    verify(groupService).createGroup(any(User.class), any(Group.class));
    assertNotNull(response);
    assertEquals(Long.valueOf("1"), response.getId());
    assertEquals(create.getName(), response.getName());
  }

  @Test
  public void updateGroup() {
    var param = mockGroupParam();
    var update = mockGroupUpdate();
    var group = mockGroup("1");
    var user = mockUser();

    when(userService.getUserByCode(anyString())).thenReturn(user);
    when(groupService.getGroupById(param.getGroupId())).thenReturn(group);
    when(groupService.updateGroup(user, group)).thenReturn(group);

    var response = fixture.updateGroup(param, update);

    verify(userService).getUserByCode(anyString());
    verify(groupService).updateGroup(any(User.class), any(Group.class));
    assertNotNull(response);
    assertEquals(param.getGroupId(), response.getId());
    assertEquals(update.getName(), response.getName());
    assertEquals(update.getDescription(), response.getDescription());
  }

  @Test
  public void leaveGroup() {
    var param = mockGroupParam();
    when(groupService.getGroupById(param.getGroupId())).thenReturn(mockGroup("1"));

    fixture.leaveGroup(param);

    verify(groupService).leaveGroup(any(User.class), any(Group.class));
  }

  @Test
  public void inviteToGroup() {
    var param = new GroupInviteParam();
    param.setUserCode("UUID1");
    param.setGroupId(1L);
    param.setUserCodes(Set.of("CODE1"));

    when(userService.getUserByCode(anyString())).thenReturn(mockUser());
    when(groupService.getGroupById(param.getGroupId())).thenReturn(mockGroup("1"));

    fixture.inviteToGroup(param);

    verify(groupService).inviteToGroup(any(User.class), any(Group.class), anySet());
  }

  @Test
  public void acceptInvite() {
    var param = mockInviteParam();
    when(userService.getUserByCode(anyString())).thenReturn(mockUser());

    fixture.acceptInvite(param);

    verify(groupService).acceptInvite(any(User.class), anyString());
  }

  @Test
  public void removeInvite() {
    var param = mockInviteParam();
    when(userService.getUserByCode(anyString())).thenReturn(mockUser());

    fixture.removeInvite(param);

    verify(groupService).removeInvite(any(User.class), anyString());
  }

  private UserParam mockUserParam() {
    var param = new UserParam();
    param.setUserCode("UUID1");
    return param;
  }

  private GroupParam mockGroupParam() {
    var param = new GroupParam();
    param.setUserCode("UUID1");
    param.setGroupId(1L);
    return param;
  }

  private InviteParam mockInviteParam() {
    var param = new InviteParam();
    param.setUserCode("UUID1");
    param.setGroupId(1L);
    param.setInviteCode("INVITE");
    return param;
  }

  private User mockUser() {
    return new User(1L, "UUID1", "userName1", "FirstName1", "LastName1",
      "user1@email.com", "PASSWORD1", OffsetDateTime.now(), OffsetDateTime.now(),
      false, new HashSet<>(), new ArrayList<>(), new ArrayList<>());
  }

  private GroupRequestV1 mockGroupCreate() {
    var group =  new GroupRequestV1();
    group.setName("GroupName1");
    group.setDescription("GroupDescription1");
    return group;
  }

  private GroupRequestV1 mockGroupUpdate() {
    var group =  new GroupRequestV1();
    group.setName("GroupNameUpdated1");
    group.setDescription("GroupDescriptionUpdated1");
    return group;
  }

  private Group mockGroup(String id) {
    return new Group(Long.valueOf(id), "GroupName".concat(id), "GroupDescription".concat(id), new HashSet<>());
  }

  private List<Group> mockGroups() {
    return Arrays.asList(mockGroup("1"), mockGroup("2"), mockGroup("3"));
  }
}
