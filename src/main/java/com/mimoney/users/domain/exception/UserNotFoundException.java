package com.mimoney.users.domain.exception;

import com.mimoney.users.domain.enumerated.ErrorCode;

public class UserNotFoundException extends EntityNotFoundException {

  public UserNotFoundException(String userCode) {
    super(ErrorCode.USER_NOT_FOUND, userCode);
  }

}
