package com.mimoney.users.api.commons;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.ServletRequest;

public class PingControllerTest {

  private final PingController fixture = new PingController();

  @Test
  public void shouldReturnPong_whenPinging() {
    Assertions.assertEquals("pong", fixture.ping(Mockito.mock(ServletRequest.class)));
  }

}
