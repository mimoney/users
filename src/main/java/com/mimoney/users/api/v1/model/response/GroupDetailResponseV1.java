package com.mimoney.users.api.v1.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Getter
@Setter
public class GroupDetailResponseV1 {

  @Schema(example = ParamExample.RESOURCE_ID)
  private Long id;

  @Schema(example = "Home Expenses")
  private String name;

  @Schema(example = "Group to share home expenses with family")
  private String description;

  private Set<UserShortResponseV1> users = new HashSet<>();

}
