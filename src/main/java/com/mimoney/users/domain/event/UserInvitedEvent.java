package com.mimoney.users.domain.event;

import com.mimoney.users.domain.model.GroupInvite;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class UserInvitedEvent extends ApplicationEvent {

  private final GroupInvite groupInvite;

  public UserInvitedEvent(GroupInvite groupInvite) {
    super(groupInvite);
    this.groupInvite = groupInvite;
  }
}
