package com.mimoney.users.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class GroupInviteId implements Serializable {

  @Column(name = "host_id", nullable = false)
  private Long hostId;

  @Column(name = "guest_id", nullable = false)
  private Long guestId;

  @Column(name = "user_group_id", nullable = false)
  private Long groupId;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof GroupInviteId)) return false;
    GroupInviteId that = (GroupInviteId) o;
    return hostId.equals(that.hostId) && guestId.equals(that.guestId) && groupId.equals(that.groupId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(hostId, guestId, groupId);
  }
}
