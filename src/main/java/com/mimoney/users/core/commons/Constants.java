package com.mimoney.users.core.commons;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

  public static final String APP = "USERS";

  public static final String PING = "/ping";
  public static final String V1 = "/v1";
  public static final String USERS = "/users";
  public static final String GROUPS = "/users/{userCode}/groups";
  public static final String RESOURCES = "/users/{userCode}/resources";

}
