package com.mimoney.users.domain.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResourceType {

  WALLET,
  PAYMENT,
  SAVINGS,
  BANK_ACCOUNT,
  LOAN,
  EMERGENCY_FUND,
  OTHERS

}
