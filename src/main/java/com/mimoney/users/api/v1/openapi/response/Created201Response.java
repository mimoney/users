package com.mimoney.users.api.v1.openapi.response;

import com.mimoney.users.api.v1.openapi.response.error.AccessDenied403Response;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

@Target({METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@AccessDenied403Response
@ApiResponse(responseCode = "201", description = "Created - Successfully completed.")
public @interface Created201Response {
}
