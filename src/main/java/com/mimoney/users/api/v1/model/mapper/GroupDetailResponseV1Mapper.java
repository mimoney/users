package com.mimoney.users.api.v1.model.mapper;

import com.mimoney.users.api.v1.model.response.GroupDetailResponseV1;
import com.mimoney.users.domain.model.Group;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class GroupDetailResponseV1Mapper extends BaseResponseV1Mapper<Group, GroupDetailResponseV1> {

  public GroupDetailResponseV1Mapper(final ModelMapper mapper) {
    super(mapper);
  }
}
