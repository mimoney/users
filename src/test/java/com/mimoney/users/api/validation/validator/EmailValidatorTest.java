package com.mimoney.users.api.validation.validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class EmailValidatorTest {

  @InjectMocks
  private EmailValidator fixture;

  @Mock
  private ConstraintValidatorContext context;

  @Test
  public void isValid() {
    assertTrue(fixture.isValid("email@test.com.br", context));
    assertTrue(fixture.isValid("user.email@mimoney.com", context));
  }

  @Test
  public void isNotValid() {
    assertFalse(fixture.isValid(null, context));
    assertFalse(fixture.isValid("", context));
    assertFalse(fixture.isValid("not_email_format_string", context));
  }
}
