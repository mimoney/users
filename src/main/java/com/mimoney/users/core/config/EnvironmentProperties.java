package com.mimoney.users.core.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Validated
@Configuration
@ConfigurationProperties(prefix = "environment", ignoreUnknownFields = false)
public class EnvironmentProperties {

  private Database database;
  private Kafka kafka;
  private OpenApi openApi;

  @Getter
  @Setter
  public static class Database {
    @NotNull
    private String host;
    @NotNull
    private Integer port;
    @NotNull
    private String user;
    private String password;
  }

  @Getter
  @Setter
  public static class Kafka {
    @NotNull
    private String host;
    @NotNull
    private Integer port;
  }

  @Getter
  @Setter
  public static class OpenApi {
    private String contactUrl;
    private String contactEmail;
    private String serverProd;
    private String serverQA;
  }

}
