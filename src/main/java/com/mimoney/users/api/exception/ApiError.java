package com.mimoney.users.api.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mimoney.users.domain.enumerated.ErrorCode;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Schema(description = "Represents api error objects")
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"errorCode", "title", "message", "objects"})
public class ApiError {

  @Schema(example = "9994", description = "Internal error code")
  private final Integer errorCode;

  @Schema(example = "Invalid Data", description = "Error title")
  private final String title;

  @Schema(example = "Something is wrong. Check the request and try again.", description = "Descriptive error message")
  private final String message;

  @Schema(description = "List of validation error objects")
  private final List<ApiErrorObject> objects;

  @Getter
  @Builder
  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonPropertyOrder({"errorCode", "name", "message"})
  public static class ApiErrorObject {

    @Schema(example = "1003", description = "Internal validation error code")
    private final Integer errorCode;

    @Schema(example = "email", description = "Invalid parameter name")
    private final String name;

    @Schema(example = "The email is invalid.", description = "Detailed error message")
    private final String message;
  }

  @JsonIgnore
  public boolean isValidationError() {
    return !CollectionUtils.isEmpty(this.objects);
  }

  @JsonIgnore
  public boolean isUnexpectedError() {
    return this.title.equals(ErrorCode.INTERNAL_SERVER_ERROR.getTitle());
  }
}
