package com.mimoney.users.api.v1.model.request.parameter;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamDescription;
import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Hidden
@Getter
@Setter
public class GroupParam extends UserParam {

  @Parameter(in = ParameterIn.PATH, description = ParamDescription.GROUP_ID, example = ParamExample.GROUP_ID)
  @NotNull
  private Long groupId;

}
