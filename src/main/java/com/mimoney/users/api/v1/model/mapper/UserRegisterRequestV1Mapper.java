package com.mimoney.users.api.v1.model.mapper;

import com.mimoney.users.api.v1.model.request.UserRegisterRequestV1;
import com.mimoney.users.domain.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserRegisterRequestV1Mapper extends BaseRequestV1Mapper<UserRegisterRequestV1, User> {

  public UserRegisterRequestV1Mapper(final ModelMapper mapper) {
    super(mapper);
  }
}
