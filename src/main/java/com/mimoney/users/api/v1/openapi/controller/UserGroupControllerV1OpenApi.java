package com.mimoney.users.api.v1.openapi.controller;

import com.mimoney.users.api.v1.model.request.GroupRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.GroupInviteParam;
import com.mimoney.users.api.v1.model.request.parameter.GroupParam;
import com.mimoney.users.api.v1.model.request.parameter.InviteParam;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.api.v1.model.response.GroupDetailResponseV1;
import com.mimoney.users.api.v1.model.response.GroupResponseV1;
import com.mimoney.users.api.v1.openapi.OpenApiConstants;
import com.mimoney.users.api.v1.openapi.response.Created201Response;
import com.mimoney.users.api.v1.openapi.response.NoContent204Response;
import com.mimoney.users.api.v1.openapi.response.Ok200Response;
import com.mimoney.users.api.v1.openapi.response.error.GlobalErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;

import java.util.List;

@Tag(name = OpenApiConstants.Tag.TAG_USERS_GROUPS, description = "User groups management operations.")
public interface UserGroupControllerV1OpenApi {

  @Ok200Response
  @GlobalErrorResponse
  @Operation(summary = "List user groups", description = "Gets a list of all groups that a user is member of by its user code.")
  List<GroupResponseV1> getUserGroups(@ParameterObject UserParam param);

  @Ok200Response
  @GlobalErrorResponse
  @Operation(summary = "Get user group by ID", description = "Gets a group detailed data by its member's user code and the group ID.")
  GroupDetailResponseV1 getGroup(@ParameterObject GroupParam param);

  @Created201Response
  @GlobalErrorResponse
  @Operation(summary = "Create group", description = "Creates a new group by its administrator's user code.")
  GroupDetailResponseV1 createGroup(@ParameterObject UserParam param, @RequestBody GroupRequestV1 body);

  @Ok200Response
  @GlobalErrorResponse
  @Operation(summary = "Update group", description = "Updates a group by its administrator's user code and the group ID.")
  GroupDetailResponseV1 updateGroup(@ParameterObject GroupParam param, @RequestBody GroupRequestV1 body);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Leave group", description = "Removes a member user from a group by its user code and the group ID.")
  void leaveGroup(@ParameterObject GroupParam param);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Invite to group", description = "Invites a list of users to be members of a group by their user codes.")
  void inviteToGroup(@ParameterObject GroupInviteParam param);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Accept invite", description = "Includes the guest as member of the group that was invited.")
  void acceptInvite(@ParameterObject InviteParam param);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Delete invite", description = "Deletes a group invitation by its host's or guest's user code and the group ID.")
  void removeInvite(@ParameterObject InviteParam param);

}
