package com.mimoney.users.domain.service;

import com.mimoney.users.domain.model.Group;
import com.mimoney.users.domain.model.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface IGroupService {

  List<Group> getUserGroups(String userCode);

  Group getGroupById(Long groupId);

  @Transactional
  Group createGroup(User user, Group group);

  @Transactional
  Group updateGroup(User user, Group group);

  @Transactional
  void leaveGroup(User user, Group group);

  @Transactional
  void inviteToGroup(User host, Group group, Set<String> guests);

  @Transactional
  void acceptInvite(User guest, String inviteCode);

  @Transactional
  void removeInvite(User user, String inviteCode);

}
