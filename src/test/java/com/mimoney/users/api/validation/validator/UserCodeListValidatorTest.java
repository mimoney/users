package com.mimoney.users.api.validation.validator;

import com.mimoney.users.api.validation.ValidUserCodeList;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.repository.IUserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class UserCodeListValidatorTest {

  private static final String CODE_1 = "CODE1";
  private static final String CODE_2 = "CODE2";
  private static final String CODE_3 = "CODE3";

  @InjectMocks
  private UserCodeListValidator fixture;
  @Mock
  private IUserRepository userRepository;
  @Mock
  private ConstraintValidatorContext context;

  @Test
  public void isValid() {
    Mockito.when(userRepository.findUsersByCodes(Mockito.anySet()))
      .thenReturn(Arrays.asList(user(CODE_1), user(CODE_2), user(CODE_3)));

    Assertions.assertTrue(fixture.isValid(Set.of(CODE_1, CODE_2, CODE_3), context));
  }

  @Test
  public void isValidWithNullArgumentAndNullable() {
    ValidUserCodeList validUserCodeList = Mockito.mock(ValidUserCodeList.class);
    Mockito.when(validUserCodeList.nullable()).thenReturn(true);
    fixture.initialize(validUserCodeList);

    Assertions.assertTrue(fixture.isValid(null, context));
  }

  @Test
  public void isNotValid() {
    Mockito.when(userRepository.findUsersByCodes(Mockito.anySet()))
      .thenReturn(Arrays.asList(user(CODE_1), user(CODE_2)));

    Assertions.assertFalse(fixture.isValid(Set.of(CODE_1, CODE_2, CODE_3), context));
  }

  private User user(String code) {
    var user = new User();
    user.setUserCode(code);
    return user;
  }
}
