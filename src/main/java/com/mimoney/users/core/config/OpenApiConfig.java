package com.mimoney.users.core.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
@RequiredArgsConstructor
public class OpenApiConfig {

  @Value("${info.build.version}")
  private String appVersion;
  private final EnvironmentProperties properties;

  @Bean
  public OpenAPI openAPI() {
    return new OpenAPI()
      .info(info())
      .servers(servers());
  }

  private Info info() {
    return new Info()
      .title("Users API Service")
      .description("MiMoney API service that handle and provide users data.")
      .contact(contact())
      .version(appVersion);
  }

  private List<Server> servers() {
    return Arrays.asList(
      new Server().description("QA").url(properties.getOpenApi().getServerQA()),
      new Server().description("Production").url(properties.getOpenApi().getServerProd())
    );
  }

  private Contact contact() {
    return new Contact()
      .name("MiMoney Development Team")
      .url(properties.getOpenApi().getContactUrl())
      .email(properties.getOpenApi().getContactEmail());
  }
}
