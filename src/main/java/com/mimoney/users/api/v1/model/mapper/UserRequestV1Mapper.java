package com.mimoney.users.api.v1.model.mapper;

import com.mimoney.users.api.v1.model.request.UserRequestV1;
import com.mimoney.users.domain.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserRequestV1Mapper extends BaseRequestV1Mapper<UserRequestV1, User> {

  public UserRequestV1Mapper(final ModelMapper mapper) {
    super(mapper);
  }
}
