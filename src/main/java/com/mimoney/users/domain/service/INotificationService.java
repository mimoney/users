package com.mimoney.users.domain.service;

import com.mimoney.users.infrastructure.messaging.dto.PasswordResetDto;
import com.mimoney.users.infrastructure.messaging.dto.UserInviteDto;
import com.mimoney.users.infrastructure.messaging.dto.UserRegisteredDto;

public interface INotificationService {

  void sendUserRegistryNotification(UserRegisteredDto userRegistered);

  void sendPasswordResetNotification(PasswordResetDto passwordReset);

  void sendGroupInviteNotification(UserInviteDto userInvite);

}
