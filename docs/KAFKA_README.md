## Kafka useful commands
#### Execute bash terminal inside Kafka container
  
      docker exec -i -t -u root mimoney_kafka /bin/bash

#### Access Kafka directory
  
      cd /usr/bin

####Commands:
- Show kafka topics list
  
      ./kafka-topics --list --zookeeper zookeeper:2181

- Open producer channel
  
      ./kafka-console-producer --broker-list kafka:29092 --topic <topic-name>

- Open consumer channel
  
      ./kafka-console-consumer --bootstrap-server kafka:29092 --from-beginning --topic <topic-name>

- Show consumer groups
  
      ./kafka-consumer-groups --bootstrap-server kafka:29092 --list
