package com.mimoney.users.core.config;

import com.mimoney.users.core.commons.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class KafkaConfig {

  public static final String USER_REGISTRY_TOPIC = "user-registry-topic";
  public static final String PASSWORD_RESET_TOPIC = "password-reset-topic";
  public static final String USER_INVITE_TOPIC = "user-invite-topic";

  private final String KAFKA_SERVER;

  public KafkaConfig(EnvironmentProperties props) {
    this.KAFKA_SERVER = String.format("%s:%d", props.getKafka().getHost(), props.getKafka().getPort());
    log.info("[{}] Establishing connection to Kafka [{}]", Constants.APP, this.KAFKA_SERVER);
  }

  @Bean
  public KafkaAdmin kafkaAdmin() {
    Map<String, Object> config = new HashMap<>();
    config.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_SERVER);
    return new KafkaAdmin(config);
  }

  @Bean
  public NewTopic userRegistryTopic() {
    return TopicBuilder.name(USER_REGISTRY_TOPIC).partitions(1).replicas(1).build();
  }

  @Bean
  public NewTopic passwordResetTopic() {
    return TopicBuilder.name(PASSWORD_RESET_TOPIC).partitions(1).replicas(1).build();
  }

  @Bean
  public NewTopic userInviteTopic() {
    return TopicBuilder.name(USER_INVITE_TOPIC).partitions(1).replicas(1).build();
  }

  @Bean
  public ProducerFactory<String, Object> producerFactory() {
    Map<String, Object> configs = new HashMap<>();
    configs.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_SERVER);
    configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
    return new DefaultKafkaProducerFactory<>(configs);
  }

  @Bean
  public KafkaTemplate<String, Object> kafkaTemplate() {
    return new KafkaTemplate<>(producerFactory());
  }

}
