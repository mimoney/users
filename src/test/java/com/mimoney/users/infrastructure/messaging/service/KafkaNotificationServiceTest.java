package com.mimoney.users.infrastructure.messaging.service;

import com.mimoney.users.infrastructure.messaging.dto.PasswordResetDto;
import com.mimoney.users.infrastructure.messaging.dto.UserInviteDto;
import com.mimoney.users.infrastructure.messaging.dto.UserRegisteredDto;
import com.mimoney.users.utils.TestUtils;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaProducerException;
import org.springframework.kafka.core.KafkaSendCallback;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class KafkaNotificationServiceTest {

  @InjectMocks
  private KafkaNotificationService fixture;

  @Mock
  private KafkaTemplate<String, Object> template;

  @Mock
  private ListenableFuture<SendResult<String, Object>> future;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private SendResult<String, Object> result;

  @BeforeEach
  void setUp() {
    when(template.send(anyString(), any())).thenReturn(future);
    lenient().when(result.getProducerRecord().value()).thenReturn(TestUtils.toObject("{ \"property\": \"propertyValue\"}"));
  }

  @Test
  void sendUserRegistryNotification() {
    answerOnSuccess();
    fixture.sendUserRegistryNotification(UserRegisteredDto.builder().build());

    verify(template).send(anyString(), any());
  }

  @Test
  void tryToSendUserRegistryNotification_thenFailAndLog() {
    answerOnFailure();
    fixture.sendUserRegistryNotification(UserRegisteredDto.builder().build());

    verify(template).send(anyString(), any());
  }

  @Test
  void sendPasswordResetNotification() {
    answerOnSuccess();
    fixture.sendPasswordResetNotification(PasswordResetDto.builder().build());

    verify(template).send(anyString(), any());
  }

  @Test
  void trySendPasswordResetNotification_thenFailAndLog() {
    answerOnFailure();
    fixture.sendPasswordResetNotification(PasswordResetDto.builder().build());

    verify(template).send(anyString(), any());
  }

  @Test
  void sendGroupInviteNotification() {
    answerOnSuccess();
    fixture.sendGroupInviteNotification(UserInviteDto.builder().build());

    verify(template).send(anyString(), any());
  }

  @Test
  void trySendGroupInviteNotification_thenFailAndLog() {
    answerOnFailure();
    fixture.sendGroupInviteNotification(UserInviteDto.builder().build());

    verify(template).send(anyString(), any());
  }

  @SuppressWarnings("unchecked")
  private void answerOnSuccess() {
    Mockito.doAnswer(invoke -> {
      ((KafkaSendCallback<String, Object>) invoke.getArgument(0)).onSuccess(result);
      return null;
    }).when(future).addCallback(any(KafkaSendCallback.class));
  }

  @SuppressWarnings("unchecked")
  private void answerOnFailure() {
    var ex = new KafkaProducerException(mock(ProducerRecord.class), "MESSGE", new Exception("EXCEPTION"));
    Mockito.doAnswer(invoke -> {
      ((KafkaSendCallback<String, Object>) invoke.getArgument(0)).onFailure(ex);
      return null;
    }).when(future).addCallback(any(KafkaSendCallback.class));
  }
}
