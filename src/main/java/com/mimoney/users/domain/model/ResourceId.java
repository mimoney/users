package com.mimoney.users.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class ResourceId implements Serializable {

  @Column(name = "user_id", nullable = false)
  private Long userId;

  @Column(name = "id", nullable = false)
  private Long id;

  public ResourceId(Long userId) {
    this.userId = userId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof ResourceId)) return false;
    ResourceId that = (ResourceId) o;
    return userId.equals(that.userId) && id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, id);
  }
}
