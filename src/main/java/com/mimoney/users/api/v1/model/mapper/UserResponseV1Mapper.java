package com.mimoney.users.api.v1.model.mapper;

import com.mimoney.users.api.v1.model.response.UserResponseV1;
import com.mimoney.users.domain.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserResponseV1Mapper extends BaseResponseV1Mapper<User, UserResponseV1> {

  public UserResponseV1Mapper(final ModelMapper mapper) {
    super(mapper);
  }
}
