package com.mimoney.users.api.v1.model.mapper;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.lang.reflect.ParameterizedType;

@Component
@RequiredArgsConstructor
public class BaseRequestV1Mapper<S, T> {

  private final ModelMapper mapper;

  public T map(S source) {
    return mapper.map(source, getTarget());
  }

  public void copy(S source, T target) {
    mapper.map(source, target);
  }

  @SuppressWarnings("unchecked")
  private Class<T> getTarget() {
    ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
    return (Class<T>) parameterizedType.getActualTypeArguments()[1];
  }
}
