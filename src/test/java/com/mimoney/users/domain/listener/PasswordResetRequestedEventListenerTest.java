package com.mimoney.users.domain.listener;

import com.mimoney.users.domain.enumerated.TokenType;
import com.mimoney.users.domain.event.PasswordResetRequestedEvent;
import com.mimoney.users.domain.model.Token;
import com.mimoney.users.domain.model.User;
import com.mimoney.users.domain.service.INotificationService;
import com.mimoney.users.infrastructure.messaging.dto.PasswordResetDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PasswordResetRequestedEventListenerTest {

  @InjectMocks
  private PasswordResetRequestedEventListener fixture;
  @Mock
  private INotificationService notificationService;

  @Test
  void handleEvent() {
    fixture.onApplicationEvent(new PasswordResetRequestedEvent(new Token(new User(), TokenType.REGISTRATION)));
    Mockito.verify(notificationService).sendPasswordResetNotification(Mockito.any(PasswordResetDto.class));
  }
}
