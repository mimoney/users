package com.mimoney.users.api.v1.openapi.response.error;

import com.mimoney.users.api.exception.ApiError;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

@Target({METHOD, TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@ApiResponse(
  responseCode = "500",
  description = "Internal System Error - An unexpected error occurred in the system. Try again and, if the error persists, contact the support.",
  content =
  @Content(
    mediaType = MediaType.APPLICATION_JSON_VALUE,
    schema = @Schema(implementation = ApiError.class),
    examples = {
      @ExampleObject(
        "{\n" +
          "  \"errorCode\": 9999,\n" +
          "  \"title\": \"Internal System Error\",\n" +
          "  \"message\": \"An unexpected internal error occurred. Please, contact the support.\"\n" +
          "}")
    })
)
public @interface InternalServerError500Response {
}
