package com.mimoney.users.api.v1.openapi.response.error;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

@Target({METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@BadRequest400Response
@AccessDenied403Response
@NotFound404ErrorResponse
@InternalServerError500Response
public @interface GlobalErrorResponse {
}
