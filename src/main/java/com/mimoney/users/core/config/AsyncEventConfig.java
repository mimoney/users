package com.mimoney.users.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

@Configuration
public class AsyncEventConfig {

  @Bean
  @Profile({"dev", "qa", "prod"})
  public ApplicationEventMulticaster applicationEventMulticaster() {
    SimpleApplicationEventMulticaster simpleMulticaster = new SimpleApplicationEventMulticaster();
    simpleMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
    return simpleMulticaster;
  }
}

