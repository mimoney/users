package com.mimoney.users.api.v1.openapi;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OpenApiConstants {

  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  public static class Tag {
    public static final String TAG_USERS = "Users";
    public static final String TAG_USERS_ACCESS = "User Access";
    public static final String TAG_USERS_RESOURCES = "User Resources";
    public static final String TAG_USERS_GROUPS = "User Groups";
  }

  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  public static class ParamDescription {
    public static final String USER_CODE = "36 characters string user code.";
    public static final String USERS_CODES = "List of 36 characters string user code, comma separated.";
    public static final String RESOURCE_ID = "User resource ID number.";
    public static final String GROUP_ID = "Group ID number.";
    public static final String TOKEN = "6 number characters string token.";
    public static final String EMAIL = "User email requesting password reset.";
    public static final String INVITE_CODE = "36 characters string invite code.";
  }

  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  public static class ParamExample {
    public static final String USER_CODE = "2647b47a-dce3-4606-a0cc-1f3c1f698222";
    public static final String USERS_CODES = "2647b47a-dce3-4606-a0cc-1f3c1f698222,828ba8df-d23e-4b30-a495-248c4ce53aa5,c8381263-4cfb-4b28-adfb-ac7fbfddcbb7";
    public static final String RESOURCE_ID = "5741484";
    public static final String GROUP_ID = "7457265";
    public static final String TOKEN = "847054";
    public static final String EMAIL = "john.doe@mimoney.com";
    public static final String USER_NAME = "john.doe";
    public static final String FIRST_NAME = "John";
    public static final String LAST_NAME = "Doe";
    public static final String PASS_EXAMPLE = "Pas$w0rd";
    public static final String INVITE_CODE = "2647b47a-dce3-4606-a0cc-1f3c1f698222";
  }
}
