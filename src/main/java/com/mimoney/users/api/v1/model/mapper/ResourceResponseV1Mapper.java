package com.mimoney.users.api.v1.model.mapper;

import com.mimoney.users.api.v1.model.response.ResourceResponseV1;
import com.mimoney.users.domain.model.Resource;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ResourceResponseV1Mapper extends BaseResponseV1Mapper<Resource, ResourceResponseV1> {

  public ResourceResponseV1Mapper(final ModelMapper mapper) {
    super(mapper);
  }
}
