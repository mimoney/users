package com.mimoney.users.api.v1.model.request;

import com.mimoney.users.api.validation.ValidEmail;
import com.mimoney.users.api.validation.ValidPassword;
import com.mimoney.users.api.validation.ValidPasswordConfirmation;
import com.mimoney.users.api.validation.helper.PasswordConfirmationHelper;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

import static com.mimoney.users.api.v1.openapi.OpenApiConstants.ParamExample;

@Getter
@Setter
@ValidPasswordConfirmation
public class UserRegisterRequestV1 implements PasswordConfirmationHelper {

  @Schema(example = ParamExample.FIRST_NAME)
  @NotBlank
  private String firstName;

  @Schema(example = ParamExample.LAST_NAME)
  @NotBlank
  private String lastName;

  @Schema(example = ParamExample.USER_NAME)
  @NotBlank
  private String userName;

  @Schema(example = ParamExample.EMAIL, required = true)
  @ValidEmail
  private String email;

  @Schema(example = ParamExample.PASS_EXAMPLE, required = true)
  @ValidPassword
  private String password;

  @Schema(example = ParamExample.PASS_EXAMPLE, required = true)
  private String confirmPassword;

}
