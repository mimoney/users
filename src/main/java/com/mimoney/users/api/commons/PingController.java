package com.mimoney.users.api.commons;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;

import static com.mimoney.users.core.commons.Constants.PING;

@Hidden
@Slf4j
@RestController
@RequestMapping(path = PING, produces = MediaType.APPLICATION_JSON_VALUE)
public class PingController {

  @GetMapping
  public String ping(final ServletRequest request) {
    log.debug("[Ping request received] [IP] [{}]", request.getRemoteAddr());
    return "pong";
  }

}
