package com.mimoney.users.infrastructure.messaging.service;

import com.mimoney.users.core.commons.Constants;
import com.mimoney.users.core.commons.Utils;
import com.mimoney.users.core.config.KafkaConfig;
import com.mimoney.users.domain.service.INotificationService;
import com.mimoney.users.infrastructure.messaging.dto.PasswordResetDto;
import com.mimoney.users.infrastructure.messaging.dto.UserInviteDto;
import com.mimoney.users.infrastructure.messaging.dto.UserRegisteredDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaProducerException;
import org.springframework.kafka.core.KafkaSendCallback;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaNotificationService implements INotificationService {

  private final KafkaTemplate<String, Object> kafkaTemplate;

  @Override
  public void sendUserRegistryNotification(UserRegisteredDto userRegistered) {
    ListenableFuture<SendResult<String, Object>> future =
      kafkaTemplate.send(KafkaConfig.USER_REGISTRY_TOPIC, userRegistered);

    future.addCallback(new KafkaSendCallback<>() {
      @Override
      public void onSuccess(SendResult<String, Object> result) {
        log.info("[{}] [User registered notification sent] [SUCCESS] [{}]", Constants.APP, Utils.toJson(result.getProducerRecord().value()));
      }

      @Override
      public void onFailure(KafkaProducerException ex) {
        log.error("[{}] [User registered notification sent] [FAILURE] [{}]", Constants.APP, Utils.toJson(ex.getFailedProducerRecord().value()), ex);
      }
    });
  }

  @Override
  public void sendPasswordResetNotification(PasswordResetDto passwordReset) {
    ListenableFuture<SendResult<String, Object>> future =
      kafkaTemplate.send(KafkaConfig.PASSWORD_RESET_TOPIC, passwordReset);

    future.addCallback(new KafkaSendCallback<>() {
      @Override
      public void onSuccess(SendResult<String, Object> result) {
        log.info("[{}] [Password reset requested notification sent] [SUCCESS] [{}]", Constants.APP, Utils.toJson(result.getProducerRecord().value()));
      }

      @Override
      public void onFailure(KafkaProducerException ex) {
        log.error("[{}] [Password reset requested notification sent] [FAILURE] [{}]", Constants.APP, Utils.toJson(ex.getFailedProducerRecord().value()), ex);
      }
    });
  }

  @Override
  public void sendGroupInviteNotification(UserInviteDto userInvite) {
    ListenableFuture<SendResult<String, Object>> future =
      kafkaTemplate.send(KafkaConfig.USER_INVITE_TOPIC, userInvite);

    future.addCallback(new KafkaSendCallback<>() {
      @Override
      public void onSuccess(SendResult<String, Object> result) {
        log.info("[{}] [User invite notification sent] [SUCCESS] [{}]", Constants.APP, Utils.toJson(result.getProducerRecord().value()));
      }

      @Override
      public void onFailure(KafkaProducerException ex) {
        log.error("[{}] [User invite notification sent] [FAILURE] [{}]", Constants.APP, Utils.toJson(ex.getFailedProducerRecord().value()), ex);
      }
    });
  }
}
