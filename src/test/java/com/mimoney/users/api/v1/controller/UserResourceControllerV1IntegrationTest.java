package com.mimoney.users.api.v1.controller;

import com.mimoney.users.UsersApiApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.mimoney.users.utils.TestUtils.getResource;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = UsersApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserResourceControllerV1IntegrationTest extends AbstractIntegrationTest {

  private MockMvc mvc;

  @BeforeEach
  public void setUp() {
    mvc = setupMvc();
    runTestData();
  }

  @Test
  public void shouldReturn200_whenGetResourceList() throws Exception {
    mvc.perform(
      get(_V1_USERS_CODE_RESOURCES, VALID_USER_CODE)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/resources/resource-list-response.json")));
  }

  @Test
  public void shouldReturn201_whenAddResource() throws Exception {
    mvc.perform(
      post(_V1_USERS_CODE_RESOURCES, VALID_USER_CODE)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(getResource("data/request/v1/resources/resource-create-body.json"))
    ).andExpect(status().isCreated())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/resources/created-resource-response.json")));
  }

  @Test
  public void shouldReturn200_whenUpdateUser() throws Exception {
    mvc.perform(
      put(_V1_USERS_CODE_RESOURCES + _RESOURCE_ID, VALID_USER_CODE, 1L)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(getResource("data/request/v1/resources/resource-update-body.json"))
    ).andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/response/v1/resources/updated-resource-response.json")));
  }

  @Test
  public void shouldReturn204_whenRemoveResource() throws Exception {
    mvc.perform(
      delete(_V1_USERS_CODE_RESOURCES + _RESOURCE_ID, VALID_USER_CODE, 1L)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().isNoContent());
  }

  @Test
  public void shouldReturn404_givenInvalidID() throws Exception {
    mvc.perform(
      put(_V1_USERS_CODE_RESOURCES + _RESOURCE_ID, VALID_USER_CODE, 10000L)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(getResource("data/request/v1/resources/resource-update-body.json"))
    ).andExpect(status().isNotFound())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(content().json(getResource("data/error-response/resource-not-found.json")));
  }

  @Test
  public void shouldReturn406_givenInvalidAcceptHeader() throws Exception {
    mvc.perform(get(_V1_USERS_CODE_RESOURCES, VALID_USER_CODE).accept(MediaType.APPLICATION_XML)).andExpect(status().isNotAcceptable());
  }
}
