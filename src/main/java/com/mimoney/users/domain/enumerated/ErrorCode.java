package com.mimoney.users.domain.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

@Slf4j
@Getter
@AllArgsConstructor
public enum ErrorCode {

// Validation error codes
  INVALID_PASSWORD(1001, Title.INVALID_DATA, null, HttpStatus.BAD_REQUEST),
  INVALID_PASSWORD_CONFIRMATION(1002, Title.INVALID_DATA, "The passwords are not equal.", HttpStatus.BAD_REQUEST),
  INVALID_EMAIL(1003, Title.INVALID_DATA, "The email is invalid.", HttpStatus.BAD_REQUEST),
  INVALID_USER_CODE(1004, Title.INVALID_DATA, null, HttpStatus.BAD_REQUEST),

// Rule violation error codes
  USER_EMAIL_EXISTS(2001, Title.RULE_VIOLATION, "There already is an account using that email address.", HttpStatus.BAD_REQUEST),
  USER_NAME_EXISTS(2002, Title.RULE_VIOLATION, "There already is an account using that user name.", HttpStatus.BAD_REQUEST),
  USER_ENTITY_IN_USE(2003, Title.RULE_VIOLATION, "This user is currently in use and cannot be deleted.", HttpStatus.CONFLICT),
  TOKEN_EXPIRED(2005, Title.RULE_VIOLATION, "The informed token has expired.", HttpStatus.FORBIDDEN),
  USER_EMAIL_NOT_EXISTS(2006, Title.RULE_VIOLATION, "There is no account using that email address.", HttpStatus.BAD_REQUEST),

// Not found error codes
  USER_NOT_FOUND(3001, Title.NOT_FOUND, "User '%s' not found.", HttpStatus.NOT_FOUND),
  GROUP_NOT_FOUND(3002, Title.NOT_FOUND, "Group '%s' not found.", HttpStatus.NOT_FOUND),
  RESOURCE_NOT_FOUND(3003, Title.NOT_FOUND, "Resource '%s' not found in the User '%s' resources.", HttpStatus.NOT_FOUND),
  TOKEN_NOT_FOUND(3004, Title.NOT_FOUND, "Token '%s' not found.", HttpStatus.NOT_FOUND),
  INVITE_NOT_FOUND(3005, Title.NOT_FOUND, "Invite '%s' not found.", HttpStatus.NOT_FOUND),

// Generic error codes
  BUSINESS_RULE_ERROR(9990, Title.RULE_VIOLATION, "A business rule violation occurred.", HttpStatus.BAD_REQUEST),
  ACCESS_DENIED(9992, Title.ACCESS_DENIED, "You do not have permission to execute this action.", HttpStatus.FORBIDDEN),
  NOT_FOUND(9993, Title.NOT_FOUND, "The requested resource '%s' does not exist.", HttpStatus.BAD_REQUEST),
  INVALID_DATA(9994, Title.INVALID_DATA, "Something is wrong. Check the request and try again.", HttpStatus.BAD_REQUEST),
  INVALID_BODY(9995, Title.INVALID_DATA, "The request body is invalid. Check for syntax errors.", HttpStatus.BAD_REQUEST),
  INVALID_PARAM(9996, Title.INVALID_DATA, "The parameter '%s' received the value '%s', that is invalid. The correct type is '%s'.", HttpStatus.BAD_REQUEST),
  INVALID_PROPERTY(9997, Title.INVALID_DATA, "The property '%s' received the value '%s', that is invalid. The correct type is '%s'.", HttpStatus.BAD_REQUEST),
  NONEXISTENT_PROPERTY(9998, Title.INVALID_DATA, "The property '%s' does not exist. Check the request and try again.", HttpStatus.BAD_REQUEST),
  INTERNAL_SERVER_ERROR(9999, Title.INTERNAL_SYSTEM_ERROR, "An unexpected internal error occurred. Please, contact the support.", HttpStatus.INTERNAL_SERVER_ERROR);

  private final int code;
  private final String title;
  private final String message;
  private final HttpStatus status;

  private static final class Title {

    public static final String INVALID_DATA = "Invalid Data";
    public static final String RULE_VIOLATION = "Rule Violation";
    public static final String NOT_FOUND = "Not Found";
    public static final String ACCESS_DENIED = "Access Denied";
    public static final String INTERNAL_SYSTEM_ERROR = "Internal System Error";

  }

}
