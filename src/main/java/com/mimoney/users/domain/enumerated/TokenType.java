package com.mimoney.users.domain.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TokenType {

  REGISTRATION,
  PASSWORD

}
