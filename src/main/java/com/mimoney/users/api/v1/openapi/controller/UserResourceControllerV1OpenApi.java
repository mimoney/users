package com.mimoney.users.api.v1.openapi.controller;

import com.mimoney.users.api.v1.model.request.ResourceRequestV1;
import com.mimoney.users.api.v1.model.request.parameter.ResourceParam;
import com.mimoney.users.api.v1.model.request.parameter.UserParam;
import com.mimoney.users.api.v1.model.response.ResourceResponseV1;
import com.mimoney.users.api.v1.openapi.OpenApiConstants;
import com.mimoney.users.api.v1.openapi.response.Created201Response;
import com.mimoney.users.api.v1.openapi.response.NoContent204Response;
import com.mimoney.users.api.v1.openapi.response.Ok200Response;
import com.mimoney.users.api.v1.openapi.response.error.GlobalErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;

import java.util.List;

@Tag(name = OpenApiConstants.Tag.TAG_USERS_RESOURCES, description = "User resources management operations.")
public interface UserResourceControllerV1OpenApi {

  @Ok200Response
  @GlobalErrorResponse
  @Operation(summary = "List user resources", description = "Gets a list of all user's resources by its user code.")
  List<ResourceResponseV1> getUserResources(@ParameterObject UserParam param);

  @Created201Response
  @GlobalErrorResponse
  @Operation(summary = "Add resource", description = "Adds an user resource by its user code.")
  ResourceResponseV1 addUserResource(@ParameterObject UserParam param, @RequestBody ResourceRequestV1 body);

  @Ok200Response
  @GlobalErrorResponse
  @Operation(summary = "Update resource", description = "Updates an user resource by its user code and ID.")
  ResourceResponseV1 updateUserResource(@ParameterObject ResourceParam param, @RequestBody ResourceRequestV1 body);

  @NoContent204Response
  @GlobalErrorResponse
  @Operation(summary = "Delete resource", description = "Removes an user resource by its user code and ID.")
  void removeUserResource(@ParameterObject ResourceParam param);

}
